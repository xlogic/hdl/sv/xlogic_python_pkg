#!/usr/bin/env python3
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

def dummy():
    pass

def arg_1_pass(arg0):
    pass

def arg_1_return(arg0):
    print(arg0)
    return arg0

def arg_2_pass(arg0, arg1):
    pass

def arg_2_return(arg0, arg1):
    return arg0

class Dummy:
    def __init__(self):
        pass

    def dummy(self):
        pass

    def arg_1_pass(self, arg0):
        pass

    def arg_1_return(self, arg0):
        return arg0
