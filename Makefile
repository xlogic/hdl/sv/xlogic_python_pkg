# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

PROJECT_DIR  := $(patsubst %/,%,$(strip $(dir $(realpath $(lastword $(MAKEFILE_LIST))))))
TESTS_DIR    := $(PROJECT_DIR)/tests
PROJECT_NAME := xlogic_python_pkg
OUTPUT_DIR   ?= $(PWD)/build
COMMA        := ,

PYTHON_INCLUDE_DIR := $(shell python3 -c 'import sysconfig; print(sysconfig.get_config_var("INCLUDEPY"))')
PYTHON_LIBRARY_DIR := $(shell python3 -c 'import sysconfig; print(sysconfig.get_config_var("LIBDIR"))')
PYTHON_LIBRARY     := $(subst lib,,$(subst .so,,$(subst .a,,$(shell python3 -c 'import sysconfig; print(sysconfig.get_config_var("LIBRARY"))'))))

C_SOURCES   := $(wildcard $(PROJECT_DIR)/src/*.c)
C_HEADERS   := $(wildcard $(PROJECT_DIR)/src/*.h)
C_INCLUDES  := $(sort $(realpath $(dir $(C_HEADERS)))) $(PYTHON_INCLUDE_DIR)
C_OBJECTS   := $(addsuffix .o,$(subst $(PROJECT_DIR),$(OUTPUT_DIR),$(C_SOURCES)))
C_LIBRARIES := $(PYTHON_LIBRARY) $(PROJECT_NAME)
C_LIBRARIES_DIR := $(realpath $(PYTHON_LIBRARY_DIR)) $(OUTPUT_DIR)

C_COMPILER ?= $(shell which gcc)
C_COMPILER_FLAGS := \
	-c \
	-O3 \
	-Wall \
	-Wextra \
	-fPIC \
	$(addprefix -I,$(C_INCLUDES)) \
	-o

C_LINKER ?= $(C_COMPILER)
C_LINKER_FLAGS := -shared -fPIC -o

SV_SOURCES  := $(wildcard $(PROJECT_DIR)/src/*.sv)
SV_HEADERS  := $(wildcard $(PROJECT_DIR)/src/*.svh)
SV_INCLUDES := $(sort $(realpath $(dir $(SV_HEADERS))))
SV_OBJECTS  := $(addsuffix .o,$(subst $(PROJECT_DIR),$(OUTPUT_DIR),$(SV_SOURCES)))

SV_LINKER_FLAGS := \
	$(addprefix -L,$(C_LIBRARIES_DIR)) \
	$(addprefix -Wl$(COMMA)--rpath ,$(C_LIBRARIES_DIR)) \
	$(addprefix -l,$(C_LIBRARIES))

OBJECTS := $(SV_OBJECTS) $(C_OBJECTS) $(OUTPUT_DIR)/lib$(PROJECT_NAME).so

all: run

include $(PROJECT_DIR)/scripts/toolchain.mk

$(OUTPUT_DIR)/src/$(PROJECT_NAME).sv.o: $(OUTPUT_DIR)/src/xlogic_pkg.sv.o

$(OUTPUT_DIR)/src/$(PROJECT_NAME)_tb.sv.o: $(OUTPUT_DIR)/src/$(PROJECT_NAME).sv.o

$(OUTPUT_DIR)/%.sv.o: $(PROJECT_DIR)/%.sv $(SV_HEADERS)
	@mkdir -p "$(dir $@)"&& echo "Compiling $(subst $(PROJECT_DIR)/,,$<)" && rm -rf "$@" && \
	$(SV_COMPILER) $(SV_COMPILER_FLAGS) "$<" && touch "$@"

$(OUTPUT_DIR)/%.c.o: $(PROJECT_DIR)/%.c $(C_HEADERS)
	@mkdir -p "$(dir $@)" && echo "Compiling $(subst $(PROJECT_DIR)/,,$<)" && \
	$(C_COMPILER) $(C_COMPILER_FLAGS) "$@" "$<"

$(OUTPUT_DIR)/lib$(PROJECT_NAME).so: $(C_OBJECTS)
	@mkdir -p "$(dir $@)" && $(C_LINKER) $(C_LINKER_FLAGS) "$@" $^

compile: $(OBJECTS)

run: $(OBJECTS)
	@PYTHONPATH="$(TESTS_DIR)" $(SV_RUNTIME) $(SV_RUNTIME_FLAGS) $(PROJECT_NAME)_tb

clean:
	@rm -rf "$(OUTPUT_DIR)" xcelium.d

.PHONY: all compile run clean
