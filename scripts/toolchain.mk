#!/usr/bin/env make
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

CURRENT_DIR  := $(patsubst %/,%,$(strip $(dir $(realpath $(lastword $(MAKEFILE_LIST))))))
COMMAND_XRUN := $(strip $(shell command -v xrun 2>/dev/null || true))
COMMAND_VSIM := $(strip $(shell command -v vsim 2>/dev/null || true))

# Load default toolchain
include $(CURRENT_DIR)/questa.mk

ifneq (,$(COMMAND_XRUN))
include $(CURRENT_DIR)/xcelium.mk
endif

ifneq (,$(COMMAND_VSIM))
include $(CURRENT_DIR)/questa.mk
endif
