#!/usr/bin/env make
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

SV_COMPILER := xrun
SV_COMPILER_FLAGS := \
	-disable_sem2009 \
	-elaborate \
	-l "$(OUTPUT_DIR)/xrun.log" \
	-history_file "$(OUTPUT_DIR)/xrun.history" \
	$(addprefix -incdir ,$(SV_INCLUDES))

SV_RUNTIME := xrun
SV_RUNTIME_FLAGS := \
	-Q \
	-disable_sem2009 \
	-l "$(OUTPUT_DIR)/xrun.log" \
	-history_file "$(OUTPUT_DIR)/xrun.history" \
	-sv_root "$(PYTHON_LIBRARY_DIR)" \
	-sv_lib  "lib$(PYTHON_LIBRARY)" \
	-sv_root "$(OUTPUT_DIR)" \
	-sv_lib  "lib$(PROJECT_NAME)" \
	-top
