#!/usr/bin/env make
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

SV_COMPILER := vlog
SV_COMPILER_FLAGS := \
	-quiet \
	-work "$(OUTPUT_DIR)" \
	-cpppath "$(C_COMPILER)" \
	$(addprefix +incdir+,$(SV_INCLUDES))

SV_RUNTIME := xrun
SV_RUNTIME_FLAGS := \
	-quiet \
	-suppress 3812 \
	-work "$(OUTPUT_DIR)" \
	-wlf "$(OUTPUT_DIR)/waveform.wlf" \
	-l "$(OUTPUT_DIR)/log" \
	-cpppath "$(C_COMPILER)" \
	-ldflags "$(SV_LINKER_FLAGS)" \
	-c \
	-do 'log -r /*; run 100; exit'
