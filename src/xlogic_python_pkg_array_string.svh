// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_ARRAY_STRING_SVH
`define XLOGIC_PYTHON_PKG_ARRAY_STRING_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_str.svh"
`include "xlogic_python_pkg_array.svh"

class array_string extends array#(string);
    extern static function items_t create(chandle pyobject);

    extern static function void create_ref(ref items_t items, input chandle pyobject);

    extern protected virtual function void set(ref items_t items, int index, input chandle value);

    extern protected virtual function void set_bytes(ref items_t items, const ref bytes_t bytes);

    local static array_string m_instance;
endclass

function array_string::items_t array_string::create(chandle pyobject);
    create_ref(create, pyobject);
endfunction

function void array_string::create_ref(ref items_t items, input chandle pyobject);
    array#(string)::make(m_instance, items, pyobject);
endfunction

function void array_string::set(ref items_t items, int index, input chandle value);
    items[index] = xlogic_python_pkg_PyUnicode_Check(value) ? str::from_python(value).to_string() : "";
endfunction

function void array_string::set_bytes(ref items_t items, const ref bytes_t bytes);
    if (items.size() != bytes.size()) begin
        items = new[bytes.size()];
    end

    foreach (bytes[index]) begin
        items[index] = bytes[index];
    end
endfunction

`endif /* XLOGIC_PYTHON_PKG_ARRAY_STRING_SVH */
