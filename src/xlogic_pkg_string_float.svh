// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PKG_STRING_FLOAT_SVH
`define XLOGIC_PKG_STRING_FLOAT_SVH

`include "xlogic_pkg_string_integer.svh"

class string_float#(type T = real);
    extern static function T convert(const ref string s);
endclass

function string_float::T string_float::convert(const ref string s);
    real value = 0;

    if ((s.len() >= 3) && (s[0] == "0") && (s[1] != ".") && (s[1] != "e") && (s[1] != "E")) begin
        return string_integer#(longint)::convert(s);
    end

    if ($sscanf(s, "%f", value) >= 1) begin
        return value;
    end

    return 0;
endfunction

`endif /* XLOGIC_PKG_STRING_FLOAT_SVH */
