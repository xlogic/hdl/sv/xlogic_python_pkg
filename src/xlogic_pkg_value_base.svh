// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PKG_VALUE_BASE_SVH
`define XLOGIC_PKG_VALUE_BASE_SVH

`include "xlogic_pkg_value.svh"

class value_base implements value;
    typedef longint unsigned longint_unsigned_t;
    typedef int unsigned int_unsigned_t;
    typedef value::bytes_t bytes_t;
    typedef value::array_t array_t;
    typedef value::kind_t kind_t;
    typedef value::map_t map_t;

    extern virtual function kind_t kind();

    extern virtual function int size();

    extern virtual function value at(int index);

    extern virtual function value get(string key);

    extern virtual function value get_ref(const ref string key);

    extern virtual function void get_map(ref map_t items);

    extern virtual function void get_bytes(ref bytes_t items);

    extern virtual function void get_array(ref array_t items);

    extern virtual function void get_string(ref string value);

    extern virtual function map_t to_map();

    extern virtual function bytes_t to_bytes();

    extern virtual function array_t to_array();

    extern virtual function string to_string();

    extern virtual function complex_t to_complex();

    extern virtual function real to_real();

    extern virtual function shortreal to_shortreal();

    extern virtual function bit to_bit();

    extern virtual function int to_int();

    extern virtual function longint to_longint();

    extern virtual function int unsigned to_int_unsigned();

    extern virtual function longint unsigned to_longint_unsigned();
endclass

function value::kind_t value_base::kind();
    return value::NULL;
endfunction

function int value_base::size();
    return 0;
endfunction

function value value_base::at(int index);
    return null;
endfunction

function value value_base::get(string key);
    return null;
endfunction

function value value_base::get_ref(const ref string key);
    return null;
endfunction

function void value_base::get_map(ref map_t items);
    items = '{default: null};
endfunction

function void value_base::get_bytes(ref bytes_t items);
    items = {};
endfunction

function void value_base::get_array(ref array_t items);
    items = {};
endfunction

function void value_base::get_string(ref string value);
    value = "";
endfunction

function value::map_t value_base::to_map();
    return '{default: null};
endfunction

function value::bytes_t value_base::to_bytes();
    return {};
endfunction

function value::array_t value_base::to_array();
    return {};
endfunction

function string value_base::to_string();
    return "";
endfunction

function complex_t value_base::to_complex();
    return '{default: 0};
endfunction

function real value_base::to_real();
    return 0;
endfunction

function shortreal value_base::to_shortreal();
    return 0;
endfunction

function bit value_base::to_bit();
    return 1'b0;
endfunction

function int value_base::to_int();
    return 0;
endfunction

function longint value_base::to_longint();
    return 0;
endfunction

function int unsigned value_base::to_int_unsigned();
    return 0;
endfunction

function longint unsigned value_base::to_longint_unsigned();
    return 0;
endfunction

`endif /* XLOGIC_PKG_VALUE_BASE_SVH */
