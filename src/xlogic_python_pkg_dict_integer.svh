// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_DICT_INTEGER_SVH
`define XLOGIC_PYTHON_PKG_DICT_INTEGER_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_object.svh"

virtual class dict_integer#(type T = int);
    typedef T items_t[string];

    extern static function object create(items_t items = '{default: 0});

    extern static function object create_ref(const ref items_t items);
endclass

function object dict_integer::create(items_t items = '{default: 0});
    return create_ref(items);
endfunction

function object dict_integer::create_ref(const ref items_t items);
    object pyobject = object::create(xlogic_python_pkg_PyDict_New());

    if (pyobject.handle() == null) begin
        return pyobject;
    end

    foreach (items[key]) begin
        void'(xlogic_python_pkg_PyDict_SetItemString(
            pyobject.handle(),
            key,
            pyobject.steal_reference(xlogic_python_pkg_PyLong_FromLongLong(items[key]))
        ));
    end

    return pyobject;
endfunction

`endif /* XLOGIC_PYTHON_PKG_DICT_INTEGER_SVH */
