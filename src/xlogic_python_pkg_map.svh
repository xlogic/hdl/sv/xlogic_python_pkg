// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_MAP_SVH
`define XLOGIC_PYTHON_PKG_MAP_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_str.svh"

virtual class map#(type T = int);
    typedef map#(T) object_t;
    typedef T items_t[string];

    protected pure virtual function void set(ref items_t items, const ref string key, input chandle value);

    extern static function void make(object_t object, ref items_t items, input chandle pyobject);

    extern local static function void dict(object_t object, ref items_t items, input chandle pyobject);

    extern local static function void mapping(object_t object, ref items_t items, input chandle pyobject);
endclass

function void map::make(object_t object, ref items_t items, input chandle pyobject);
    if (items.size()) begin
        items.delete();
    end

    if (xlogic_python_pkg_PyDict_Check(pyobject)) begin
        dict(object, items, pyobject);
    end else if (xlogic_python_pkg_PyMapping_Check(pyobject)) begin
        mapping(object, items, pyobject);
    end
endfunction

function void map::dict(object_t object, ref items_t items, input chandle pyobject);
    chandle pyitems = xlogic_python_pkg_PyDict_Items(pyobject);
    longint size;

    if (pyitems == null) begin
        return;
    end

    size = xlogic_python_pkg_PyList_GET_SIZE(pyitems);

    for (longint index = 0; index < size; index++) begin
        chandle pyitem  = xlogic_python_pkg_PyList_GET_ITEM(pyitems, index);
        chandle pykey   = xlogic_python_pkg_PyTuple_GET_ITEM(pyitem, 0);
        chandle pyvalue = xlogic_python_pkg_PyTuple_GET_ITEM(pyitem, 1);
        string  key     = str::from_python(pykey).to_string();

        object.set(items, key, pyvalue);
    end

    xlogic_python_pkg_Py_DECREF(pyitems);
endfunction

function void map::mapping(object_t object, ref items_t items, input chandle pyobject);
    chandle pymap = xlogic_python_pkg_PyMapping_Items(pyobject);
    chandle pyseq;
    longint size;

    if (pymap == null) begin
        return;
    end

    pyseq = xlogic_python_pkg_PySequence_Fast(pyobject);

    if (pyseq == null) begin
        xlogic_python_pkg_Py_DECREF(pymap);
        return;
    end

    size = xlogic_python_pkg_PySequence_Fast_GET_SIZE(pyseq);

    for (longint index = 0; index < size; index++) begin
        chandle pyitem  = xlogic_python_pkg_PySequence_Fast_GET_ITEM(pyseq, index);
        chandle pykey   = xlogic_python_pkg_PyTuple_GET_ITEM(pyitem, 0);
        chandle pyvalue = xlogic_python_pkg_PyTuple_GET_ITEM(pyitem, 1);
        string  key     = str::from_python(pykey).to_string();

        object.set(items, key, pyvalue);
    end

    xlogic_python_pkg_Py_DECREF(pyseq);
    xlogic_python_pkg_Py_DECREF(pymap);
endfunction

`endif /* XLOGIC_PYTHON_PKG_MAP_SVH */
