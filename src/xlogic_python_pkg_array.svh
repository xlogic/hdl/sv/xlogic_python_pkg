// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_ARRAY_SVH
`define XLOGIC_PYTHON_PKG_ARRAY_SVH

`include "xlogic_python_pkg_dpi.svh"

virtual class array#(type T = int);
    typedef array#(T) object_t;
    typedef byte bytes_t[];
    typedef T items_t[];

    protected pure virtual function void set(ref items_t items, int index, input chandle value);

    protected pure virtual function void set_bytes(ref items_t items, const ref bytes_t bytes);

    extern static function void make(object_t object, ref items_t items, input chandle pyobject);

    extern local static function void list(object_t object, ref items_t items, input chandle pyobject);

    extern local static function void tuple(object_t object, ref items_t items, input chandle pyobject);

    extern local static function void bytes(object_t object, ref items_t items, input chandle pyobject);

    extern local static function void byte_array(object_t object, ref items_t items, input chandle pyobject);

    extern local static function void seq(object_t object, ref items_t items, input chandle pyobject);
endclass

function void array::make(object_t object, ref items_t items, input chandle pyobject);
    if (xlogic_python_pkg_PyList_Check(pyobject)) begin
        list(object, items, pyobject);
    end else if (xlogic_python_pkg_PyTuple_Check(pyobject)) begin
        tuple(object, items, pyobject);
    end else if (xlogic_python_pkg_PyBytes_Check(pyobject)) begin
        bytes(object, items, pyobject);
    end else if (xlogic_python_pkg_PyByteArray_Check(pyobject)) begin
        byte_array(object, items, pyobject);
    end else if (xlogic_python_pkg_PySequence_Check(pyobject)) begin
        seq(object, items, pyobject);
    end else begin
        items.delete();
    end
endfunction

function void array::list(object_t object, ref items_t items, input chandle pyobject);
    longint size = xlogic_python_pkg_PyList_GET_SIZE(pyobject);

    if (items.size() != size) begin
        items = new[size];
    end

    for (int index = 0; index < size; index++) begin
        object.set(items, index, xlogic_python_pkg_PyList_GET_ITEM(pyobject, index));
    end
endfunction

function void array::tuple(object_t object, ref items_t items, input chandle pyobject);
    longint size = xlogic_python_pkg_PyTuple_GET_SIZE(pyobject);

    if (items.size() != size) begin
        items = new[size];
    end

    for (int index = 0; index < size; index++) begin
        object.set(items, index, xlogic_python_pkg_PyTuple_GET_ITEM(pyobject, index));
    end
endfunction

function void array::bytes(object_t object, ref items_t items, input chandle pyobject);
    bytes_t bytes = bytes_t'(xlogic_python_pkg_PyBytes_AsString(pyobject));

    object.set_bytes(items, bytes);
endfunction

function void array::byte_array(object_t object, ref items_t items, input chandle pyobject);
    bytes_t bytes = bytes_t'(xlogic_python_pkg_PyByteArray_AsString(pyobject));

    object.set_bytes(items, bytes);
endfunction

function void array::seq(object_t object, ref items_t items, input chandle pyobject);
    longint size = xlogic_python_pkg_PySequence_Size(pyobject);

    if (items.size() != size) begin
        items = new[size];
    end

    for (int index = 0; index < size; index++) begin
        chandle item = xlogic_python_pkg_PySequence_GetItem(pyobject, index);

        object.set(items, index, item);

        xlogic_python_pkg_Py_DECREF(item);
    end
endfunction

`endif /* XLOGIC_PYTHON_PKG_ARRAY_SVH */
