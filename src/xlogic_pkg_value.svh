// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PKG_VALUE_SVH
`define XLOGIC_PKG_VALUE_SVH

`include "xlogic_pkg_types.svh"

interface class value;
    typedef enum int {
        NULL,
        INTEGER,
        BOOLEAN,
        NUMBER,
        STRING,
        OBJECT,
        ARRAY,
        ERROR
    } kind_t;

    typedef byte bytes_t[];
    typedef value array_t[];
    typedef value map_t[string];

    pure virtual function kind_t kind();

    pure virtual function int size();

    pure virtual function value at(int index);

    pure virtual function value get(string key);

    pure virtual function value get_ref(const ref string key);

    pure virtual function void get_map(ref map_t items);

    pure virtual function void get_bytes(ref bytes_t items);

    pure virtual function void get_array(ref array_t items);

    pure virtual function void get_string(ref string value);

    pure virtual function map_t to_map();

    pure virtual function bytes_t to_bytes();

    pure virtual function array_t to_array();

    pure virtual function string to_string();

    pure virtual function complex_t to_complex();

    pure virtual function real to_real();

    pure virtual function shortreal to_shortreal();

    pure virtual function bit to_bit();

    pure virtual function int to_int();

    pure virtual function longint to_longint();

    pure virtual function int unsigned to_int_unsigned();

    pure virtual function longint unsigned to_longint_unsigned();
endclass

`endif /* XLOGIC_PKG_VALUE_SVH */
