// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_METHOD_SVH
`define XLOGIC_PYTHON_PKG_METHOD_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"
`include "xlogic_python_pkg_tuple.svh"
`include "xlogic_python_pkg_dict.svh"
`include "xlogic_python_pkg_interpreter.svh"
`include "xlogic_python_pkg_object_call.svh"

function automatic chandle pymethod_ref(chandle object, const ref string name, const ref value::array_t args, const ref value::map_t kwargs);
    chandle callable = xlogic_python_pkg_PyObject_GetAttrString(object, name);

    if (!xlogic_python_pkg_PyCallable_Check(callable)) begin
        return null;
    end

    return object_call_ref(callable, args, kwargs);
endfunction

function automatic chandle pymethod(chandle object, string name, value::array_t args = {}, value::map_t kwargs = '{default: null});
    return pymethod_ref(object, name, args, kwargs);
endfunction

function automatic value method_ref(chandle object, const ref string name, const ref value::array_t args, const ref value::map_t kwargs);
    chandle result = pymethod_ref(object, name, args, kwargs);

    method_ref = value::from_python(result);

    xlogic_python_pkg_Py_XDECREF(result);
endfunction

function automatic value method(chandle object, string name, value::array_t args = {}, value::map_t kwargs = '{default: null});
    return method_ref(object, name, args, kwargs);
endfunction

`endif /* XLOGIC_PYTHON_PKG_METHOD_SVH */
