// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

// Package: xlogic_pkg
package xlogic_pkg;
    `include "xlogic_pkg_types.svh"
    `include "xlogic_pkg_value.svh"
    `include "xlogic_pkg_value_base.svh"
    `include "xlogic_pkg_string_integer.svh"
    `include "xlogic_pkg_string_float.svh"
endpackage
