// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_INTERPRETER_SVH
`define XLOGIC_PYTHON_PKG_INTERPRETER_SVH

`include "xlogic_python_pkg_dpi.svh"

class interpreter;
    extern function new();

    extern static function interpreter create();

    extern static function int finalize();

    extern static function bit is_initialized();

    extern static function string version();

    extern static function void print_error();

    extern static function chandle import_module(const ref string name);

    extern static function chandle import_attribute(const ref string name, input bit callable = 1'b0);

    extern local static function chandle get_module_from_cache(const ref string name);

    extern local static function void cache_module(const ref string name, chandle handle);

    extern local static function chandle get_attribute_from_cache(const ref string name);

    extern local static function void cache_attribute(const ref string name, chandle handle);

    extern local static function int find_last_dot(const ref string name);

    local static chandle m_modules[string];
    local static chandle m_attributes[string];
    local static semaphore m_semaphore  = new(1);
    local static interpreter m_instance = new();
endclass

function interpreter::new();
    if (m_semaphore == null) begin
        m_semaphore = new(1);
    end

    while (!m_semaphore.try_get());

    if (xlogic_python_pkg_Py_IsInitialized() == 0) begin
        xlogic_python_pkg_Py_InitializeEx(0);
    end

    m_semaphore.put();
endfunction

function interpreter interpreter::create();
    create = new();
endfunction

function int interpreter::finalize();
    int status = 0;

    while (!m_semaphore.try_get());

    foreach (m_attributes[name]) begin
        xlogic_python_pkg_Py_DECREF(m_attributes[name]);
    end

    m_attributes.delete();

    foreach (m_modules[name]) begin
        xlogic_python_pkg_Py_DECREF(m_modules[name]);
    end

    m_modules.delete();

    if (xlogic_python_pkg_Py_IsInitialized() > 0) begin
        status = xlogic_python_pkg_Py_FinalizeEx();
    end

    m_semaphore.put();

    return status;
endfunction

function bit interpreter::is_initialized();
    return xlogic_python_pkg_Py_IsInitialized() > 0;
endfunction

function string interpreter::version();
    return xlogic_python_pkg_Py_GetVersion();
endfunction

function void interpreter::print_error();
    xlogic_python_pkg_PyErr_Print();
endfunction

function chandle interpreter::import_module(const ref string name);
    chandle handle = get_module_from_cache(name);
    chandle pyname = null;

    if (handle != null) begin
        return handle;
    end

    pyname = xlogic_python_pkg_PyUnicode_DecodeFSDefault(name);

    if (pyname == null) begin
        return null;
    end

    handle = xlogic_python_pkg_PyImport_Import(pyname);
    xlogic_python_pkg_Py_DECREF(pyname);

    if (handle != null) begin
        cache_module(name, handle);
    end

    return handle;
endfunction

function chandle interpreter::import_attribute(const ref string name, input bit callable = 1'b0);
    chandle handle = get_attribute_from_cache(name);
    chandle pymodule = null;
    string attribute_name;
    string module_name;
    int dot;

    if (handle != null) begin
        return handle;
    end

    dot = find_last_dot(name);
    module_name = name.substr(0, dot - 1);
    attribute_name = name.substr(dot + 1, name.len() - 1);

    pymodule = import_module(module_name);

    if (pymodule == null) begin
        return null;
    end

    handle = xlogic_python_pkg_PyObject_GetAttrString(pymodule, attribute_name);

    if (handle != null) begin
        if (callable && (xlogic_python_pkg_PyCallable_Check(handle) == 0)) begin
            return null;
        end

        cache_attribute(name, handle);
    end

    return handle;
endfunction

function chandle interpreter::get_module_from_cache(const ref string name);
    chandle handle = null;

    while (!m_semaphore.try_get());

    if (m_modules.exists(name)) begin
        handle = m_modules[name];
    end

    m_semaphore.put();

    return handle;
endfunction

function void interpreter::cache_module(const ref string name, chandle handle);
    while (!m_semaphore.try_get());

    if (!m_modules.exists(name)) begin
        m_modules[name] = handle;
    end

    m_semaphore.put();
endfunction

function chandle interpreter::get_attribute_from_cache(const ref string name);
    chandle handle = null;

    while (!m_semaphore.try_get());

    if (m_attributes.exists(name)) begin
        handle = m_attributes[name];
    end

    m_semaphore.put();

    return handle;
endfunction

function void interpreter::cache_attribute(const ref string name, chandle handle);
    while (!m_semaphore.try_get());

    if (!m_attributes.exists(name)) begin
        m_attributes[name] = handle;
    end

    m_semaphore.put();
endfunction

function int interpreter::find_last_dot(const ref string name);
    for (int index = name.len() - 1; index >= 0; index--) begin
        if (name[index] == ".") begin
            return index;
        end
    end

    return 0;
endfunction

`endif /* XLOGIC_PYTHON_PKG_INTERPRETER_SVH */
