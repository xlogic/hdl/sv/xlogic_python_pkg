// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_BYTES_SVH
`define XLOGIC_PYTHON_PKG_BYTES_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"
`include "xlogic_python_pkg_number.svh"

class bytes extends value;
    typedef byte items_t[];

    extern function new(byte items[] = {});

    extern static function bytes create(byte items[] = {});

    extern static function bytes create_ref(const ref byte items[]);

    extern static function bytes from_python(chandle handle);

    extern virtual function type_t get_type();

    extern virtual function xlogic_pkg::value::kind_t kind();

    extern virtual function int size();

    extern virtual function value at(int index);

    extern virtual function value get(string key);

    extern virtual function value get_ref(const ref string key);

    extern virtual function void get_map(ref map_t items);

    extern virtual function void get_bytes(ref bytes_t items);

    extern virtual function void get_array(ref array_t items);

    extern virtual function map_t to_map();

    extern virtual function bytes_t to_bytes();

    extern virtual function array_t to_array();

    extern virtual function void get_string(ref string value);

    extern virtual function string to_string();

    extern virtual function bit to_bit();

    extern virtual function chandle to_python(ref chandle borrows[$]);

    protected items_t m_items;
endclass

function bytes::new(byte items[] = {});
    m_items = items;
endfunction

function bytes bytes::create(byte items[] = {});
    create = new(items);
endfunction

function bytes bytes::create_ref(const ref byte items[]);
    create_ref = new();
    create_ref.m_items = items;
endfunction

function bytes bytes::from_python(chandle handle);
    from_python = new();
    from_python.m_items = new [xlogic_python_pkg_PyBytes_GET_SIZE(handle)] (
        items_t'(xlogic_python_pkg_PyBytes_AS_STRING(handle))
    );
endfunction

function type_t bytes::get_type();
    return BYTES;
endfunction

function xlogic_pkg::value::kind_t bytes::kind();
    return xlogic_pkg::value::ARRAY;
endfunction

function int bytes::size();
    return m_items.size();
endfunction

function value bytes::at(int index);
    return number::create(m_items[index]);
endfunction

function value bytes::get(string key);
    return number::create(m_items[key.atoi()]);
endfunction

function value bytes::get_ref(const ref string key);
    return number::create(m_items[key.atoi()]);
endfunction

function void bytes::get_map(ref map_t items);
    items.delete();

    foreach (m_items[index]) begin
        items[$sformatf("%0d", index)] = number::create(m_items[index]);
    end
endfunction

function void bytes::get_bytes(ref bytes_t items);
    items = m_items;
endfunction

function void bytes::get_array(ref array_t items);
    if (items.size() != m_items.size()) begin
        items = new [m_items.size()];
    end

    foreach (m_items[index]) begin
        items[index] = number::create(m_items[index]);
    end
endfunction

function value::map_t bytes::to_map();
    foreach (m_items[index]) begin
        to_map[$sformatf("%0d", index)] = number::create(m_items[index]);
    end
endfunction

function value::array_t bytes::to_array();
    to_array = new [m_items.size()];

    foreach (m_items[index]) begin
        to_array[index] = number::create(m_items[index]);
    end
endfunction

function value::bytes_t bytes::to_bytes();
    return m_items;
endfunction

function void bytes::get_string(ref string value);
    if (m_items.size() == 0) begin
        value = "b''";
        return;
    end

    value = "b'";

    foreach (m_items[index]) begin
        value = {value, $sformatf("\\x%02h", m_items[index])};
    end

    value = {value, "'"};
endfunction

function string bytes::to_string();
    get_string(to_string);
endfunction

function bit bytes::to_bit();
    return m_items.size() != 0;
endfunction

function chandle bytes::to_python(ref chandle borrows[$]);
    return xlogic_python_pkg_PyBytes_FromStringAndSize(string'(m_items), m_items.size());
endfunction

`endif /* XLOGIC_PYTHON_PKG_BYTES_SVH */
