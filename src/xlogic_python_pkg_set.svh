// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_SET_SVH
`define XLOGIC_PYTHON_PKG_SET_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"
`include "xlogic_python_pkg_list.svh"

class set extends list;
    extern function new(array_t items = {});

    extern static function set create(array_t items = {});

    extern static function set create_ref(const ref array_t items);

    extern static function set from_python(chandle handle);

    extern virtual function type_t get_type();

    extern virtual function xlogic_pkg::value::kind_t kind();

    extern virtual function void get_string(ref string value);

    extern virtual function string to_string();

    extern virtual function chandle to_python(ref chandle borrows[$]);
endclass

function set::new(array_t items = {});
    super.new(items);
endfunction

function set set::create(array_t items = {});
    create = new();
    create.m_items = items;
endfunction

function set set::create_ref(const ref array_t items);
    create_ref = new();
    create_ref.m_items = items;
endfunction

function set set::from_python(chandle handle);
    from_python = new();
    from_python.m_items = new [xlogic_python_pkg_PySet_Size(handle)];

    foreach (from_python.m_items[index]) begin
        chandle item = xlogic_python_pkg_PySequence_GetItem(handle, index);

        from_python.m_items[index] = value::from_python(item);

        xlogic_python_pkg_Py_DECREF(item);
    end
endfunction

function type_t set::get_type();
    return SET;
endfunction

function xlogic_pkg::value::kind_t set::kind();
    return xlogic_pkg::value::ARRAY;
endfunction

function void set::get_string(ref string value);
    _get_string(m_items, value, 'h7B, 'h7D);
endfunction

function string set::to_string();
    _get_string(m_items, to_string, 'h7B, 'h7D);
endfunction

function chandle set::to_python(ref chandle borrows[$]);
    chandle handle = xlogic_python_pkg_PyTuple_New(m_items.size());

    if (handle == null) begin
        return null;
    end

    foreach (m_items[index]) begin
        value item;

        if ($cast(item, m_items[index])) begin
            xlogic_python_pkg_PyTuple_SET_ITEM(handle, index, item.to_python(borrows));
        end else begin
            xlogic_python_pkg_PyTuple_SET_ITEM(handle, index, xlogic_python_pkg_Py_RETURN_NONE());
        end
    end

    borrows.push_back(handle);

    return xlogic_python_pkg_PySet_New(handle);
endfunction

`endif /* XLOGIC_PYTHON_PKG_SET_SVH */
