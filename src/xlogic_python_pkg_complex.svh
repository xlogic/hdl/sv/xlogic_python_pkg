// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_COMPLEX_SVH
`define XLOGIC_PYTHON_PKG_COMPLEX_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"

class complex extends value;
    extern function new(real re = 0, real im = 0);

    extern static function complex create(real re = 0, real im = 0);

    extern static function complex from_python(chandle handle);

    extern virtual function type_t get_type();

    extern virtual function xlogic_pkg::value::kind_t kind();

    extern virtual function void get_string(ref string value);

    extern virtual function string to_string();

    extern virtual function complex_t to_complex();

    extern virtual function real to_real();

    extern virtual function shortreal to_shortreal();

    extern virtual function bit to_bit();

    extern virtual function int to_int();

    extern virtual function longint to_longint();

    extern virtual function int unsigned to_int_unsigned();

    extern virtual function longint unsigned to_longint_unsigned();

    extern virtual function chandle to_python(ref chandle borrows[$]);

    protected real m_re;
    protected real m_im;
endclass

function complex::new(real re = 0, real im = 0);
    m_re = re;
    m_im = im;
endfunction

function complex complex::create(real re = 0, real im = 0);
    create = new(re, im);
endfunction

function complex complex::from_python(chandle handle);
    from_python = new(
        xlogic_python_pkg_PyComplex_RealAsDouble(handle),
        xlogic_python_pkg_PyComplex_ImagAsDouble(handle)
    );
endfunction

function type_t complex::get_type();
    return COMPLEX;
endfunction

function xlogic_pkg::value::kind_t complex::kind();
    return xlogic_pkg::value::NUMBER;
endfunction

function void complex::get_string(ref string value);
    value = $sformatf("{re: %0f, im: %0f}", m_re, m_im);
endfunction

function string complex::to_string();
    return $sformatf("{re: %0f, im: %0f}", m_re, m_im);
endfunction

function complex_t complex::to_complex();
    return '{re: m_re, im: m_im};
endfunction

function real complex::to_real();
    return m_re;
endfunction

function shortreal complex::to_shortreal();
    return shortreal'(m_re);
endfunction

function bit complex::to_bit();
    return bit'(m_re > 0);
endfunction

function int complex::to_int();
    return int'(m_re);
endfunction

function longint complex::to_longint();
    return longint'(m_re);
endfunction

function int unsigned complex::to_int_unsigned();
    return int_unsigned_t'(m_re);
endfunction

function longint unsigned complex::to_longint_unsigned();
    return longint_unsigned_t'(m_re);
endfunction

function chandle complex::to_python(ref chandle borrows[$]);
    return xlogic_python_pkg_PyComplex_FromDoubles(m_re, m_im);
endfunction

`endif /* XLOGIC_PYTHON_PKG_COMPLEX_SVH */
