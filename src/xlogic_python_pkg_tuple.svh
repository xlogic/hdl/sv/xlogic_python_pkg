// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_TUPLE_SVH
`define XLOGIC_PYTHON_PKG_TUPLE_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"
`include "xlogic_python_pkg_list.svh"

class tuple extends list;
    extern function new(array_t items = {});

    extern static function tuple create(array_t items = {});

    extern static function tuple create_ref(const ref array_t items);

    extern static function tuple from_python(chandle handle);

    extern static function tuple from_python_sequence(chandle handle);

    extern virtual function type_t get_type();

    extern virtual function xlogic_pkg::value::kind_t kind();

    extern virtual function void get_string(ref string value);

    extern virtual function string to_string();

    extern virtual function chandle to_python(ref chandle borrows[$]);

    extern static function chandle to_python_ref(const ref array_t items, ref chandle borrows[$]);
endclass

function tuple::new(array_t items = {});
    super.new(items);
endfunction

function tuple tuple::create(array_t items = {});
    create = new();
    create.m_items = items;
endfunction

function tuple tuple::create_ref(const ref array_t items);
    create_ref = new();
    create_ref.m_items = items;
endfunction

function tuple tuple::from_python(chandle handle);
    from_python = new();
    from_python.m_items = new [xlogic_python_pkg_PyTuple_GET_SIZE(handle)];

    foreach (from_python.m_items[index]) begin
        from_python.m_items[index] = value::from_python(xlogic_python_pkg_PyTuple_GET_ITEM(handle, index));
    end
endfunction

function tuple tuple::from_python_sequence(chandle handle);
    from_python_sequence = new();
    from_python_sequence.m_items = new [xlogic_python_pkg_PySequence_Size(handle)];

    foreach (from_python_sequence.m_items[index]) begin
        chandle item = xlogic_python_pkg_PySequence_GetItem(handle, index);

        from_python_sequence.m_items[index] = value::from_python(item);

        xlogic_python_pkg_Py_DECREF(item);
    end
endfunction

function type_t tuple::get_type();
    return TUPLE;
endfunction

function xlogic_pkg::value::kind_t tuple::kind();
    return xlogic_pkg::value::ARRAY;
endfunction

function chandle tuple::to_python(ref chandle borrows[$]);
    return to_python_ref(m_items, borrows);
endfunction

function void tuple::get_string(ref string value);
    _get_string(m_items, value, 'h28, 'h29);
endfunction

function string tuple::to_string();
    _get_string(m_items, to_string, 'h28, 'h29);
endfunction

function chandle tuple::to_python_ref(const ref array_t items, ref chandle borrows[$]);
    chandle handle = xlogic_python_pkg_PyTuple_New(items.size());

    if (handle == null) begin
        return null;
    end

    foreach (items[index]) begin
        value item;

        if ($cast(item, items[index])) begin
            xlogic_python_pkg_PyTuple_SET_ITEM(handle, index, item.to_python(borrows));
        end else begin
            xlogic_python_pkg_PyTuple_SET_ITEM(handle, index, xlogic_python_pkg_Py_RETURN_NONE());
        end
    end

    return handle;
endfunction

`endif /* XLOGIC_PYTHON_PKG_TUPLE_SVH */
