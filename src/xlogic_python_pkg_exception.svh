// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_EXCEPTION_SVH
`define XLOGIC_PYTHON_PKG_EXCEPTION_SVH

`include "xlogic_python_pkg_types.svh"

class exception extends value;
    extern function new(string message = "");

    extern static function exception create(string message = "");

    extern virtual function type_t get_type();

    extern virtual function xlogic_pkg::value::kind_t kind();

    extern virtual function chandle to_python(ref chandle borrows[$]);

    extern virtual function void get_string(ref string value);

    extern virtual function string to_string();

    protected string m_message;
endclass

function exception::new(string message = "");
    m_message = message;
endfunction

function exception exception::create(string message = "");
    create = new();
endfunction

function type_t exception::get_type();
    return EXCEPTION;
endfunction

function xlogic_pkg::value::kind_t exception::kind();
    return xlogic_pkg::value::ERROR;
endfunction

function chandle exception::to_python(ref chandle borrows[$]);
    return null;
endfunction

function void exception::get_string(ref string value);
    value = m_message;
endfunction

function string exception::to_string();
    return m_message;
endfunction

`endif /* XLOGIC_PYTHON_PKG_EXCEPTION_SVH */
