// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_TYPES_SVH
`define XLOGIC_PYTHON_PKG_TYPES_SVH

typedef enum int {
    NONE,
    BOOL,
    INT,
    FLOAT,
    COMPLEX,
    TUPLE,
    LIST,
    STR,
    SET,
    DICT,
    BYTES,
    BYTE_ARRAY,
    EXCEPTION
} type_t;

typedef longint ssize_t;
typedef xlogic_pkg::complex_t complex_t;

`endif /* XLOGIC_PYTHON_PKG_TYPES_SVH */
