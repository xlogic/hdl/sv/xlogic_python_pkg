// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_NUMBER_SVH
`define XLOGIC_PYTHON_PKG_NUMBER_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"

class number extends value;
    extern function new(longint value = 0);

    extern static function number create(longint value = 0);

    extern static function number from_python(chandle handle);

    extern virtual function type_t get_type();

    extern virtual function xlogic_pkg::value::kind_t kind();

    extern virtual function void get_string(ref string value);

    extern virtual function string to_string();

    extern virtual function complex_t to_complex();

    extern virtual function real to_real();

    extern virtual function shortreal to_shortreal();

    extern virtual function bit to_bit();

    extern virtual function int to_int();

    extern virtual function longint to_longint();

    extern virtual function int unsigned to_int_unsigned();

    extern virtual function longint unsigned to_longint_unsigned();

    extern virtual function chandle to_python(ref chandle borrows[$]);

    protected longint m_value;
endclass

function number::new(longint value = 0);
    m_value = value;
endfunction

function number number::create(longint value = 0);
    create = new(value);
endfunction

function number number::from_python(chandle handle);
    from_python = new(xlogic_python_pkg_PyLong_AsLongLong(handle));
endfunction

function type_t number::get_type();
    return INT;
endfunction

function xlogic_pkg::value::kind_t number::kind();
    return xlogic_pkg::value::INTEGER;
endfunction

function void number::get_string(ref string value);
    value = $sformatf("%0d", m_value);
endfunction

function string number::to_string();
    return $sformatf("%0d", m_value);
endfunction

function complex_t number::to_complex();
    return '{re: real'(m_value), default: 0};
endfunction

function real number::to_real();
    return real'(m_value);
endfunction

function shortreal number::to_shortreal();
    return shortreal'(m_value);
endfunction

function bit number::to_bit();
    return bit'(m_value > 0);
endfunction

function int number::to_int();
    return int'(m_value);
endfunction

function longint number::to_longint();
    return m_value;
endfunction

function int unsigned number::to_int_unsigned();
    return int_unsigned_t'(m_value);
endfunction

function longint unsigned number::to_longint_unsigned();
    return longint_unsigned_t'(m_value);
endfunction

function chandle number::to_python(ref chandle borrows[$]);
    return xlogic_python_pkg_PyLong_FromLongLong(m_value);
endfunction

`endif /* XLOGIC_PYTHON_PKG_NUMBER_SVH */
