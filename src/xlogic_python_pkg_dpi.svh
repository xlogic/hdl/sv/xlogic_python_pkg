// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_DPI_SVH
`define XLOGIC_PYTHON_PKG_DPI_SVH

import "DPI-C" function void xlogic_python_pkg_Py_Initialize();

import "DPI-C" function void xlogic_python_pkg_Py_InitializeEx(int initsigs);

import "DPI-C" function void xlogic_python_pkg_Py_Finalize();

import "DPI-C" function int xlogic_python_pkg_Py_FinalizeEx();

import "DPI-C" function int xlogic_python_pkg_Py_IsInitialized();

import "DPI-C" function string xlogic_python_pkg_Py_GetVersion();

import "DPI-C" function void xlogic_python_pkg_PyErr_Print();

import "DPI-C" function chandle xlogic_python_pkg_PyImport_Import(chandle name);

import "DPI-C" function chandle xlogic_python_pkg_PyObject_GetAttrString(chandle o, string attr_name);

import "DPI-C" function int xlogic_python_pkg_PyCallable_Check(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyObject_Call(chandle callable, chandle args, chandle kwargs);

import "DPI-C" function int xlogic_python_pkg_PyTuple_Check(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyTuple_New(longint len);

import "DPI-C" function void xlogic_python_pkg_PyTuple_SET_ITEM(chandle p, longint i, chandle o);

import "DPI-C" function longint xlogic_python_pkg_PyTuple_GET_SIZE(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyTuple_GET_ITEM(chandle o, longint i);

import "DPI-C" function int xlogic_python_pkg_PySet_Check(chandle o);

import "DPI-C" function longint xlogic_python_pkg_PySet_Size(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PySet_New(chandle iterable);

import "DPI-C" function int xlogic_python_pkg_PyDict_Check(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyDict_New();

import "DPI-C" function longint xlogic_python_pkg_PyDict_Size(chandle o);

import "DPI-C" function int xlogic_python_pkg_PyDict_SetItemString(chandle p, string key, chandle val);

import "DPI-C" function chandle xlogic_python_pkg_PyDict_Items(chandle o);

import "DPI-C" function int xlogic_python_pkg_PyDict_Next(chandle p, output longint ppos, output chandle pkey, output chandle pvalue);

import "DPI-C" function void xlogic_python_pkg_Py_DECREF(chandle o);

import "DPI-C" function void xlogic_python_pkg_Py_XDECREF(chandle o);

import "DPI-C" function int xlogic_python_pkg_PyLong_Check(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyLong_FromLongLong(longint v);

import "DPI-C" function longint xlogic_python_pkg_PyLong_AsLongLong(chandle o);

import "DPI-C" function int xlogic_python_pkg_PyUnicode_Check(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyUnicode_AsASCIIString(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyUnicode_FromString(string s);

import "DPI-C" function chandle xlogic_python_pkg_PyUnicode_DecodeFSDefault(string s);

import "DPI-C" function int xlogic_python_pkg_PyBytes_Check(chandle o);

import "DPI-C" function longint xlogic_python_pkg_PyBytes_Size(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyBytes_FromString(string v);

import "DPI-C" function chandle xlogic_python_pkg_PyBytes_FromStringAndSize(string v, longint len);

import "DPI-C" function string xlogic_python_pkg_PyBytes_AsString(chandle o);

import "DPI-C" function string xlogic_python_pkg_PyBytes_AS_STRING(chandle o);

import "DPI-C" function longint xlogic_python_pkg_PyBytes_GET_SIZE(chandle o);

import "DPI-C" function int xlogic_python_pkg_PyByteArray_Check(chandle o);

import "DPI-C" function longint xlogic_python_pkg_PyByteArray_Size(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyByteArray_FromStringAndSize(string v, longint len);

import "DPI-C" function string xlogic_python_pkg_PyByteArray_AsString(chandle o);

import "DPI-C" function string xlogic_python_pkg_PyByteArray_AS_STRING(chandle o);

import "DPI-C" function longint xlogic_python_pkg_PyByteArray_GET_SIZE(chandle o);

import "DPI-C" function int xlogic_python_pkg_PyFloat_Check(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyFloat_FromDouble(real v);

import "DPI-C" function real xlogic_python_pkg_PyFloat_AsDouble(chandle o);

import "DPI-C" function int xlogic_python_pkg_PyComplex_Check(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyComplex_FromDoubles(real re, real im);

import "DPI-C" function real xlogic_python_pkg_PyComplex_RealAsDouble(chandle o);

import "DPI-C" function real xlogic_python_pkg_PyComplex_ImagAsDouble(chandle o);

import "DPI-C" function int xlogic_python_pkg_PyBool_Check(chandle o);

import "DPI-C" function int xlogic_python_pkg_PyBool_AsInt(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyBool_FromLong(int v);

import "DPI-C" function int xlogic_python_pkg_PySequence_Check(chandle o);

import "DPI-C" function longint xlogic_python_pkg_PySequence_Size(chandle o);

import "DPI-C" function longint xlogic_python_pkg_PySequence_Length(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PySequence_Concat(chandle o1, chandle o2);

import "DPI-C" function chandle xlogic_python_pkg_PySequence_GetItem(chandle o, longint i);

import "DPI-C" function chandle xlogic_python_pkg_PySequence_Fast(chandle o);

import "DPI-C" function longint xlogic_python_pkg_PySequence_Fast_GET_SIZE(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PySequence_Fast_GET_ITEM(chandle o, longint i);

import "DPI-C" function int xlogic_python_pkg_PyList_Check(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyList_New(longint len);

import "DPI-C" function longint xlogic_python_pkg_PyList_Size(chandle list);

import "DPI-C" function void xlogic_python_pkg_PyList_SET_ITEM(chandle list, longint i, chandle o);

import "DPI-C" function longint xlogic_python_pkg_PyList_GET_SIZE(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyList_GET_ITEM(chandle o, longint i);

import "DPI-C" function chandle xlogic_python_pkg_PyList_AsTuple(chandle list);

import "DPI-C" function int xlogic_python_pkg_PyNone_Check(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_Py_RETURN_NONE();

import "DPI-C" function int xlogic_python_pkg_PyMapping_Check(chandle o);

import "DPI-C" function chandle xlogic_python_pkg_PyMapping_Items(chandle o);

`endif /* XLOGIC_PYTHON_PKG_DPI_SVH */
