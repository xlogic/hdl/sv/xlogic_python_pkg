// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_MAP_STRING_SVH
`define XLOGIC_PYTHON_PKG_MAP_STRING_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_str.svh"
`include "xlogic_python_pkg_map.svh"

class map_string extends map#(string);
    extern static function items_t create(chandle pyobject);

    extern static function void create_ref(ref items_t items, input chandle pyobject);

    extern protected virtual function void set(ref items_t items, const ref string key, input chandle value);

    local static map_string m_instance;
endclass

function map_string::items_t map_string::create(chandle pyobject);
    create_ref(create, pyobject);
endfunction

function void map_string::create_ref(ref items_t items, input chandle pyobject);
    map#(string)::make(m_instance, items, pyobject);
endfunction

function void map_string::set(ref items_t items, const ref string key, input chandle value);
    items[key] = xlogic_python_pkg_PyUnicode_Check(value) ? str::from_python(value).to_string() : "";
endfunction

`endif /* XLOGIC_PYTHON_PKG_MAP_STRING_SVH */
