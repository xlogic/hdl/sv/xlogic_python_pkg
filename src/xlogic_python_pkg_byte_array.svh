// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_BYTE_ARRAY_SVH
`define XLOGIC_PYTHON_PKG_BYTE_ARRAY_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"
`include "xlogic_python_pkg_bytes.svh"
`include "xlogic_python_pkg_number.svh"

class byte_array extends bytes;
    extern function new(byte items[] = {});

    extern static function byte_array create(byte items[] = {});

    extern static function byte_array create_ref(const ref byte items[]);

    extern static function byte_array from_python(chandle handle);

    extern virtual function type_t get_type();

    extern virtual function chandle to_python(ref chandle borrows[$]);
endclass

function byte_array::new(byte items[] = {});
    super.new(items);
endfunction

function byte_array byte_array::create(byte items[] = {});
    create = new(items);
endfunction

function byte_array byte_array::create_ref(const ref byte items[]);
    create_ref = new();
    create_ref.m_items = items;
endfunction

function byte_array byte_array::from_python(chandle handle);
    from_python = new();
    from_python.m_items = new [xlogic_python_pkg_PyByteArray_GET_SIZE(handle)] (
        items_t'(xlogic_python_pkg_PyByteArray_AS_STRING(handle))
    );
endfunction

function type_t byte_array::get_type();
    return BYTE_ARRAY;
endfunction

function chandle byte_array::to_python(ref chandle borrows[$]);
    return xlogic_python_pkg_PyByteArray_FromStringAndSize(string'(m_items), m_items.size());
endfunction

`endif /* XLOGIC_PYTHON_PKG_BYTE_ARRAY_SVH */
