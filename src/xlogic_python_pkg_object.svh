// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_OBJECT_SVH
`define XLOGIC_PYTHON_PKG_OBJECT_SVH

`include "xlogic_python_pkg_value.svh"

class object extends value;
    extern function new(chandle handle);

    extern static function object create(chandle handle);

    extern function chandle handle();

    extern function chandle steal_reference(chandle handle);

    extern virtual function chandle to_python(ref chandle borrows[$]);

    protected chandle m_handle;
    protected chandle m_references[$];
endclass

function object::new(chandle handle);
    m_handle = handle;
endfunction

function object object::create(chandle handle);
    create = new(handle);
endfunction

function chandle object::handle();
    return m_handle;
endfunction

function chandle object::steal_reference(chandle handle);
    m_references.push_back(handle);

    return handle;
endfunction

function chandle object::to_python(ref chandle borrows[$]);
    foreach (m_references[index]) begin
        borrows.push_back(m_references[index]);
    end

    m_references.delete();

    return handle;
endfunction

`endif
