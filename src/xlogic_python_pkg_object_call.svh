// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_OBJECT_CALL_SVH
`define XLOGIC_PYTHON_PKG_OBJECT_CALL_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"
`include "xlogic_python_pkg_tuple.svh"
`include "xlogic_python_pkg_dict.svh"
`include "xlogic_python_pkg_interpreter.svh"

function automatic chandle object_call_ref(chandle callable, const ref value::array_t args, const ref value::map_t kwargs);
    chandle pykwargs = null;
    chandle pyargs   = null;
    chandle borrows[$];

    if (callable == null) begin
        return null;
    end

    pyargs = tuple::to_python_ref(args, borrows);

    if (pyargs == null) begin
        return null;
    end

    if (kwargs.size() != 0) begin
        pykwargs = dict::to_python_ref(kwargs, borrows);

        if (pykwargs == null) begin
            xlogic_python_pkg_Py_DECREF(pyargs);
            return null;
        end
    end

    object_call_ref = xlogic_python_pkg_PyObject_Call(callable, pyargs, pykwargs);

    foreach (borrows[index]) begin
        xlogic_python_pkg_Py_DECREF(borrows[index]);
    end

    xlogic_python_pkg_Py_DECREF(pyargs);
    xlogic_python_pkg_Py_XDECREF(pykwargs);

    return object_call_ref;
endfunction

function automatic chandle object_call(chandle callable, value::array_t args = {}, value::map_t kwargs = '{default: null});
    return object_call_ref(callable, args, kwargs);
endfunction

`endif /* XLOGIC_PYTHON_PKG_OBJECT_CALL_SVH */
