// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_NONE_SVH
`define XLOGIC_PYTHON_PKG_NONE_SVH

`include "xlogic_python_pkg_types.svh"

class none extends value;
    extern static function none create();

    extern virtual function type_t get_type();

    extern virtual function xlogic_pkg::value::kind_t kind();

    extern virtual function chandle to_python(ref chandle borrows[$]);

    extern virtual function void get_string(ref string value);

    extern virtual function string to_string();
endclass

function none none::create();
    create = new();
endfunction

function type_t none::get_type();
    return NONE;
endfunction

function xlogic_pkg::value::kind_t none::kind();
    return xlogic_pkg::value::NULL;
endfunction

function chandle none::to_python(ref chandle borrows[$]);
    return xlogic_python_pkg_Py_RETURN_NONE();
endfunction

function void none::get_string(ref string value);
    value = "None";
endfunction

function string none::to_string();
    return "None";
endfunction


`endif /* XLOGIC_PYTHON_PKG_NONE_SVH */
