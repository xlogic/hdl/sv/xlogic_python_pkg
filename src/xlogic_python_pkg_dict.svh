// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_DICT_SVH
`define XLOGIC_PYTHON_PKG_DICT_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"
`include "xlogic_python_pkg_str.svh"

class dict extends value;
    extern function new(map_t items = '{default: null});

    extern static function dict create(map_t items = '{default: null});

    extern static function dict create_ref(const ref map_t items);

    extern static function dict from_python(chandle handle);

    extern static function dict from_python_mapping(chandle handle);

    extern virtual function type_t get_type();

    extern virtual function xlogic_pkg::value::kind_t kind();

    extern virtual function int size();

    extern virtual function xlogic_pkg::value at(int index);

    extern virtual function xlogic_pkg::value get(string key);

    extern virtual function xlogic_pkg::value get_ref(const ref string key);

    extern virtual function void get_map(ref map_t items);

    extern virtual function void get_array(ref array_t items);

    extern virtual function map_t to_map();

    extern virtual function array_t to_array();

    extern virtual function void get_string(ref string value);

    extern virtual function string to_string();

    extern virtual function bit to_bit();

    extern virtual function chandle to_python(ref chandle borrows[$]);

    extern static function chandle to_python_ref(const ref map_t items, ref chandle borrows[$]);

    protected map_t m_items;
endclass

function dict::new(map_t items = '{default: null});
    m_items = items;
endfunction

function dict dict::create(map_t items = '{default: null});
    create = new();
    create.m_items = items;
endfunction

function dict dict::from_python(chandle handle);
    chandle items = xlogic_python_pkg_PyDict_Items(handle);
    longint size;

    from_python = new();

    if (items == null) begin
        return from_python;
    end

    size = xlogic_python_pkg_PyList_GET_SIZE(items);

    for (longint index = 0; index < size; index++) begin
        chandle item = xlogic_python_pkg_PyList_GET_ITEM(items, index);
        chandle key = xlogic_python_pkg_PyTuple_GET_ITEM(item, 0);
        chandle val = xlogic_python_pkg_PyTuple_GET_ITEM(item, 1);

        from_python.m_items[str::from_python(key).to_string()] = value::from_python(val);
    end

    xlogic_python_pkg_Py_DECREF(items);
endfunction

function dict dict::from_python_mapping(chandle handle);
    chandle map = xlogic_python_pkg_PyMapping_Items(handle);
    chandle items;
    longint size;

    from_python_mapping = new();

    if (map == null) begin
        return from_python_mapping;
    end

    items = xlogic_python_pkg_PySequence_Fast(map);

    if (items == null) begin
        xlogic_python_pkg_Py_DECREF(map);
        return from_python_mapping;
    end

    size = xlogic_python_pkg_PySequence_Fast_GET_SIZE(items);

    for (longint index = 0; index < size; index++) begin
        chandle item = xlogic_python_pkg_PySequence_Fast_GET_ITEM(items, index);
        chandle key = xlogic_python_pkg_PyTuple_GET_ITEM(item, 0);
        chandle val = xlogic_python_pkg_PyTuple_GET_ITEM(item, 1);

        from_python_mapping.m_items[str::from_python(key).to_string()] = value::from_python(val);
    end

    xlogic_python_pkg_Py_DECREF(items);
    xlogic_python_pkg_Py_DECREF(map);
endfunction

function dict dict::create_ref(const ref map_t items);
    create_ref = new();
    create_ref.m_items = items;
endfunction

function type_t dict::get_type();
    return DICT;
endfunction

function xlogic_pkg::value::kind_t dict::kind();
    return xlogic_pkg::value::OBJECT;
endfunction

function int dict::size();
    return m_items.size();
endfunction

function xlogic_pkg::value dict::at(int index);
    return m_items[$sformatf("%0d", index)];
endfunction

function xlogic_pkg::value dict::get(string key);
    return m_items[key];
endfunction

function xlogic_pkg::value dict::get_ref(const ref string key);
    return m_items[key];
endfunction

function void dict::get_map(ref map_t items);
    items = m_items;
endfunction

function void dict::get_array(ref array_t items);
    int index = 0;

    if (items.size() != m_items.size()) begin
        items = new [m_items.size()];
    end

    foreach (m_items[key]) begin
        items[index++] = m_items[key];
    end
endfunction

function value::map_t dict::to_map();
    return m_items;
endfunction

function value::array_t dict::to_array();
    int index = 0;

    to_array = new [m_items.size()];

    foreach (m_items[key]) begin
        to_array[index++] = m_items[key];
    end
endfunction

function void dict::get_string(ref string value);
    int index = 0;
    string item;

    if (m_items.size() == 0) begin
        value = "{}";
        return;
    end

    value = "{";

    foreach (m_items[key]) begin
        m_items[key].get_string(item);

        if (m_items[key].kind() == xlogic_pkg::value::STRING) begin
            value = {value, $sformatf("%0s\"%0s\": \"%0s\"", (index != 0) ? ", " : "", key, item)};
        end else begin
            value = {value, $sformatf("%0s\"%0s\": %0s", (index != 0) ? ", " : "", key, item)};
        end

        index++;
    end

    value = {value, "}"};
endfunction

function string dict::to_string();
    get_string(to_string);
endfunction

function bit dict::to_bit();
    return m_items.size() != 0;
endfunction

function chandle dict::to_python(ref chandle borrows[$]);
    return to_python_ref(m_items, borrows);
endfunction

function chandle dict::to_python_ref(const ref map_t items, ref chandle borrows[$]);
    chandle handle = xlogic_python_pkg_PyDict_New();

    if (handle == null) begin
        return null;
    end

    foreach (items[key]) begin
        chandle val;
        value item;

        if ($cast(item, items[key])) begin
            val = item.to_python(borrows);
        end else begin
            val = xlogic_python_pkg_Py_RETURN_NONE();
        end

        borrows.push_back(val);

        void'(xlogic_python_pkg_PyDict_SetItemString(handle, key, val));
    end

    return handle;
endfunction

`endif /* XLOGIC_PYTHON_PKG_DICT_SVH */
