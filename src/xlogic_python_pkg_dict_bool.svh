// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_DICT_BOOL_SVH
`define XLOGIC_PYTHON_PKG_DICT_BOOL_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"
`include "xlogic_python_pkg_str.svh"

class dict_bool extends value;
    extern function new(const ref bit items[string]);

    extern static function dict_bool create(bit items[string] = '{default: 1'b0});

    extern static function dict_bool create_ref(const ref bit items[string]);

    extern virtual function chandle to_python(ref chandle borrows[$]);

    protected chandle m_handle;
    protected chandle m_borrows[$];
endclass

function dict_bool::new(const ref bit items[string]);
    m_handle = xlogic_python_pkg_PyDict_New();

    if (m_handle == null) begin
        return;
    end

    foreach (items[key]) begin
        chandle val = xlogic_python_pkg_PyBool_FromLong(unsigned'(items[key]));

        m_borrows.push_back(val);

        void'(xlogic_python_pkg_PyDict_SetItemString(m_handle, key, val));
    end
endfunction

function dict_bool dict_bool::create(bit items[string] = '{default: 1'b0});
    create = new(items);
endfunction

function dict_bool dict_bool::create_ref(const ref bit items[string]);
    create_ref = new(items);
endfunction

function chandle dict_bool::to_python(ref chandle borrows[$]);
    foreach (m_borrows[index]) begin
        borrows.push_back(m_borrows[index]);
    end

    m_borrows.delete();

    return m_handle;
endfunction

`endif /* XLOGIC_PYTHON_PKG_DICT_BOOL_SVH */
