// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

#include <Python.h>

#define XLOGIC_PYTHON_VERSION_3_6 0x03060000

void xlogic_python_pkg_Py_Initialize() {
    Py_Initialize();
}

void xlogic_python_pkg_Py_InitializeEx(int initsigs) {
    Py_InitializeEx(initsigs);
}

void xlogic_python_pkg_Py_Finalize() {
    Py_Finalize();
}

int xlogic_python_pkg_Py_FinalizeEx() {
#if (PY_VERSION_HEX >= XLOGIC_PYTHON_VERSION_3_6)
    return Py_FinalizeEx();
#else
    Py_Finalize(); return 0;
#endif
}

int xlogic_python_pkg_Py_IsInitialized() {
    return Py_IsInitialized();
}

const char *xlogic_python_pkg_Py_GetVersion() {
    return Py_GetVersion();
}

void xlogic_python_pkg_PyErr_Print() {
    PyErr_Print();
}

int xlogic_python_pkg_PyCallable_Check(void *o) {
    return PyCallable_Check(o);
}

void xlogic_python_pkg_Py_DECREF(void *o) {
    Py_DECREF(o);
}

void xlogic_python_pkg_Py_XDECREF(void *o) {
    Py_XDECREF(o);
}

void *xlogic_python_pkg_PyImport_Import(void *name) {
    return PyImport_Import(name);
}

void *xlogic_python_pkg_PyObject_GetAttrString(void *o, const char *attr_name) {
    return PyObject_GetAttrString(o, attr_name);
}

void *xlogic_python_pkg_PyObject_Call(void *callable, void *args, void *kwargs) {
    return PyObject_Call(callable, args, kwargs);
}

int xlogic_python_pkg_PyTuple_Check(void *o) {
    return PyTuple_Check(o);
}

void *xlogic_python_pkg_PyTuple_New(long long len) {
    return PyTuple_New(len);
}

void xlogic_python_pkg_PyTuple_SET_ITEM(void *p, long long i, void *o) {
    PyTuple_SET_ITEM(p, i, o);
}

long long xlogic_python_pkg_PyTuple_GET_SIZE(void *o) {
    return PyTuple_GET_SIZE(o);
}

void *xlogic_python_pkg_PyTuple_GET_ITEM(void *o, long long i) {
    return PyTuple_GET_ITEM(o, i);
}

int xlogic_python_pkg_PySet_Check(void *o) {
    return PySet_Check(o);
}

long long xlogic_python_pkg_PySet_Size(void *o) {
    return PySet_Size(o);
}

void *xlogic_python_pkg_PySet_New(void *iterable) {
    return PySet_New(iterable);
}

int xlogic_python_pkg_PyDict_Check(void *o) {
    return PyDict_Check(o);
}

void *xlogic_python_pkg_PyDict_New() {
    return PyDict_New();
}

long long xlogic_python_pkg_PyDict_Size(void *o) {
    return PyDict_Size(o);
}

int xlogic_python_pkg_PyDict_SetItemString(void *p, const char *key, void *val) {
    return PyDict_SetItemString(p, key, val);
}

void *xlogic_python_pkg_PyDict_Items(void *o) {
    return PyDict_Items(o);
}

int xlogic_python_pkg_PyDict_Next(void *p, long long *ppos, void **pkey, void **pvalue) {
    Py_ssize_t pos = (Py_ssize_t)(*ppos);

    int result = PyDict_Next(p, &pos, (PyObject**)pkey, (PyObject**)pvalue);

    *ppos = (long long)(pos);

    return result;
}

int xlogic_python_pkg_PyLong_Check(void *o) {
    return PyLong_Check(o);
}

void *xlogic_python_pkg_PyLong_FromLongLong(long long v) {
    return PyLong_FromLongLong(v);
}

long long xlogic_python_pkg_PyLong_AsLongLong(void *o) {
    return PyLong_AsLongLong(o);
}

int xlogic_python_pkg_PyUnicode_Check(void *o) {
    return PyUnicode_Check(o);
}

void *xlogic_python_pkg_PyUnicode_FromString(const char *s) {
    return PyUnicode_FromString(s);
}

void *xlogic_python_pkg_PyUnicode_AsASCIIString(void *o) {
    return PyUnicode_AsASCIIString(o);
}

void *xlogic_python_pkg_PyUnicode_DecodeFSDefault(const char *s) {
    return PyUnicode_DecodeFSDefault(s);
}

int xlogic_python_pkg_PyBytes_Check(void *o) {
    return PyBytes_Check(o);
}

long long xlogic_python_pkg_PyBytes_Size(void *o) {
    return PyBytes_Size(o);
}

void *xlogic_python_pkg_PyBytes_FromString(const char *v) {
    return PyBytes_FromString(v);
}

void *xlogic_python_pkg_PyBytes_FromStringAndSize(const char *v, long long len) {
    return PyBytes_FromStringAndSize(v, len);
}

const char *xlogic_python_pkg_PyBytes_AsString(void *o) {
    return PyBytes_AsString(o);
}

const char *xlogic_python_pkg_PyBytes_AS_STRING(void *o) {
    return PyBytes_AS_STRING(o);
}

long long xlogic_python_pkg_PyBytes_GET_SIZE(void *o) {
    return PyBytes_GET_SIZE(o);
}

int xlogic_python_pkg_PyByteArray_Check(void *o) {
    return PyByteArray_Check(o);
}

long long xlogic_python_pkg_PyByteArray_Size(void *o) {
    return PyByteArray_Size(o);
}

void *xlogic_python_pkg_PyByteArray_FromStringAndSize(const char *v, long long len) {
    return PyByteArray_FromStringAndSize(v, len);
}

const char *xlogic_python_pkg_PyByteArray_AsString(void *o) {
    return PyByteArray_AsString(o);
}

const char *xlogic_python_pkg_PyByteArray_AS_STRING(void *o) {
    return PyByteArray_AS_STRING(o);
}

long long xlogic_python_pkg_PyByteArray_GET_SIZE(void *o) {
    return PyByteArray_GET_SIZE(o);
}

int xlogic_python_pkg_PyFloat_Check(void *o) {
    return PyFloat_Check(o);
}

void *xlogic_python_pkg_PyFloat_FromDouble(double v) {
    return PyFloat_FromDouble(v);
}

double xlogic_python_pkg_PyFloat_AsDouble(void *o) {
    return PyFloat_AsDouble(o);
}

int xlogic_python_pkg_PyComplex_Check(void *o) {
    return PyComplex_Check(o);
}

void *xlogic_python_pkg_PyComplex_FromDoubles(double re, double im) {
    return PyComplex_FromDoubles(re, im);
}

double xlogic_python_pkg_PyComplex_RealAsDouble(void *o) {
    return PyComplex_RealAsDouble(o);
}

double xlogic_python_pkg_PyComplex_ImagAsDouble(void *o) {
    return PyComplex_ImagAsDouble(o);
}

int xlogic_python_pkg_PyBool_Check(void *o) {
    return PyBool_Check(o);
}

int xlogic_python_pkg_PyBool_AsInt(void *o) {
    return ((PyObject*)(o)) == Py_True;
}

void *xlogic_python_pkg_PyBool_FromLong(int v) {
    return PyBool_FromLong(v);
}

int xlogic_python_pkg_PySequence_Check(void *o) {
    return PySequence_Check(o);
}

long long xlogic_python_pkg_PySequence_Size(void *o) {
    return PySequence_Size(o);
}

long long xlogic_python_pkg_PySequence_Length(void *o) {
    return PySequence_Length(o);
}

void *xlogic_python_pkg_PySequence_Concat(void *o1, void *o2) {
    return PySequence_Concat(o1, o2);
}

void *xlogic_python_pkg_PySequence_GetItem(void *o, long long i) {
    return PySequence_GetItem(o, i);
}

void *xlogic_python_pkg_PySequence_Fast(void *o) {
    return PySequence_Fast(o, "Invalid Python fast sequence");
}

long long xlogic_python_pkg_PySequence_Fast_GET_SIZE(void *o) {
    return PySequence_Fast_GET_SIZE(o);
}

void *xlogic_python_pkg_PySequence_Fast_GET_ITEM(void *o, long long i) {
    return PySequence_Fast_GET_ITEM(o, i);
}

int xlogic_python_pkg_PyList_Check(void *o) {
    return PyList_Check(o);
}

void *xlogic_python_pkg_PyList_New(long long len) {
    return PyList_New(len);
}

long long xlogic_python_pkg_PyList_Size(void *list) {
    return PyList_Size(list);
}

void xlogic_python_pkg_PyList_SET_ITEM(void *list, long long i, void *o) {
    PyList_SET_ITEM(list, i, o);
}

long long xlogic_python_pkg_PyList_GET_SIZE(void *o) {
    return PyList_GET_SIZE(o);
}

void *xlogic_python_pkg_PyList_GET_ITEM(void *o, long long i) {
    return PyList_GET_ITEM(o, i);
}

void *xlogic_python_pkg_PyList_AsTuple(void *list) {
    return PyList_AsTuple(list);
}

int xlogic_python_pkg_PyNone_Check(void *o) {
    return ((PyObject*)(o)) == Py_None;
}

void *xlogic_python_pkg_Py_RETURN_NONE() {
    Py_RETURN_NONE;
}

int xlogic_python_pkg_PyMapping_Check(void *o) {
    return PyMapping_Check(o);
}

void *xlogic_python_pkg_PyMapping_Items(void *o) {
    return PyMapping_Items(o);
}
