// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_ARRAY_INTEGER_SVH
`define XLOGIC_PYTHON_PKG_ARRAY_INTEGER_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_array.svh"

class array_integer#(type T = int) extends array#(T);
    extern static function items_t create(chandle pyobject);

    extern static function void create_ref(ref items_t items, input chandle pyobject);

    extern protected virtual function void set(ref items_t items, int index, input chandle value);

    extern protected virtual function void set_bytes(ref items_t items, const ref bytes_t bytes);

    local static array_integer#(T) m_instance = new();
endclass

function array_integer::items_t array_integer::create(chandle pyobject);
    create_ref(create, pyobject);
endfunction

function void array_integer::create_ref(ref items_t items, input chandle pyobject);
    array#(T)::make(m_instance, items, pyobject);
endfunction

function void array_integer::set(ref items_t items, int index, input chandle value);
    items[index] = xlogic_python_pkg_PyLong_Check(value) ? xlogic_python_pkg_PyLong_AsLongLong(value) : 0;
endfunction

function void array_integer::set_bytes(ref items_t items, const ref bytes_t bytes);
    if (items.size() != bytes.size()) begin
        items = new[bytes.size()];
    end

    foreach (bytes[index]) begin
        items[index] = bytes[index];
    end
endfunction

`endif /* XLOGIC_PYTHON_PKG_ARRAY_INTEGER_SVH */
