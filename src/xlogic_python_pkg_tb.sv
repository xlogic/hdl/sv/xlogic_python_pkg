// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

// Module: xlogic_python_pkg_tb
// It tests the xlogic_python_pkg.
module xlogic_python_pkg_tb;
    initial begin: main
        automatic chandle object = null;
        automatic xlogic_python_pkg::array_integer#(int)::items_t array_ints = {};
        automatic xlogic_python_pkg::value value = xlogic_python_pkg::call("xlogic_python_pkg.dummy");
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        value = xlogic_python_pkg::call("nested.nested.dummy");
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        value = xlogic_python_pkg::call("nested.nested.arg_1_return", '{xlogic_python_pkg::number::create(3)});
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        value = xlogic_python_pkg::call("xlogic_python_pkg.dummy");
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        value = xlogic_python_pkg::call("xlogic_python_pkg.arg_1_pass", '{xlogic_python_pkg::number::create(5)});
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        value = xlogic_python_pkg::call("xlogic_python_pkg.arg_1_return", '{xlogic_python_pkg::number::create(5)});
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        value = xlogic_python_pkg::call("xlogic_python_pkg.arg_1_return", '{
            xlogic_python_pkg::list::create('{
                xlogic_python_pkg::number::create(1),
                xlogic_python_pkg::number::create(2),
                xlogic_python_pkg::number::create(3)
            })
        });
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        value = xlogic_python_pkg::call("xlogic_python_pkg.arg_1_return", '{
            xlogic_python_pkg::dict::create('{
                "a": xlogic_python_pkg::number::create(1),
                "b": xlogic_python_pkg::number::create(2),
                "c": xlogic_python_pkg::number::create(3),
                "d": xlogic_python_pkg::dict::create('{
                    "e": xlogic_python_pkg::list::create('{
                        xlogic_python_pkg::number::create(1)
                    }),
                    "f": xlogic_python_pkg::list::create('{
                        xlogic_python_pkg::number::create(1)
                    }),
                    "g": xlogic_python_pkg::bytes::create('{
                        'hDE, 'hAD, 'hC0, 'hDE
                    })
                })
            })
        });
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        value = xlogic_python_pkg::call("xlogic_python_pkg.arg_2_return", '{
            xlogic_python_pkg::number::create(1),
            xlogic_python_pkg::number::create(2)
        });
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        value = xlogic_python_pkg::call("xlogic_python_pkg.arg_1_return", '{
            xlogic_python_pkg::tuple::create()
        });
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        value = xlogic_python_pkg::call("xlogic_python_pkg.arg_1_return", '{
            xlogic_python_pkg::set::create()
        });
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        object = xlogic_python_pkg::create("xlogic_python_pkg.Dummy");
        $display("Object created: %0d", object != null);

        value = xlogic_python_pkg::method(object, "dummy");
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        value = xlogic_python_pkg::method(object, "arg_1_pass", '{
            xlogic_python_pkg::number::create(1)
        });
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        value = xlogic_python_pkg::method(object, "arg_1_return", '{
            xlogic_python_pkg::number::create(1)
        });
        $display("Value: %0s Type: %0s", value.to_string(), value.get_type().name());

        xlogic_python_pkg::decref(object);

        array_ints = xlogic_python_pkg::array_integer#(int)::create(xlogic_python_pkg::pycall("xlogic_python_pkg.return_list_int"));

        $display("size: %0d", array_ints.size());

        $display("Python initialized: %0s", xlogic_python_pkg::interpreter::is_initialized() ? "True": "False");
        $display("Python version: %0s", xlogic_python_pkg::interpreter::version());
        $display("Python finalize: %0d", xlogic_python_pkg::interpreter::finalize());
    end

endmodule
