// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_UTILS_SVH
`define XLOGIC_PYTHON_PKG_UTILS_SVH

function automatic void decref(chandle object);
    xlogic_python_pkg_Py_DECREF(object);
endfunction

function automatic void xdecref(chandle object);
    xlogic_python_pkg_Py_XDECREF(object);
endfunction

`endif /* XLOGIC_PYTHON_PKG_UTILS_SVH */
