// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PKG_STRING_INTEGER_SVH
`define XLOGIC_PKG_STRING_INTEGER_SVH

class string_integer#(type T = int);
    extern static function T convert(const ref string s);
endclass

function string_integer::T string_integer::convert(const ref string s);
    int len = s.len();
    T value = 0;

    if ((len < 3) || (s[0] != "0")) begin
        if ($sscanf(s, "%d", value) >= 1) begin
            return value;
        end

        return 0;
    end

    case (s[1])
    "x", "X", "h", "H": begin
        if ($sscanf(s.substr(2, len - 1), "%h", value) >= 1) begin
            return value;
        end
    end
    "o", "O": begin
        if ($sscanf(s.substr(2, len - 1), "%o", value) >= 1) begin
            return value;
        end
    end
    "b", "B": begin
        if ($sscanf(s.substr(2, len - 1), "%b", value) >= 1) begin
            return value;
        end
    end
    "d", "D": begin
        if ($sscanf(s.substr(2, len - 1), "%d", value) >= 1) begin
            return value;
        end
    end
    ".", "e", "E": begin
        real float = 0;

        if ($sscanf(s, "%f", float) >= 1) begin
            return float;
        end
    end
    endcase

    return 0;
endfunction

`endif /* XLOGIC_PKG_STRING_INTEGER_SVH */
