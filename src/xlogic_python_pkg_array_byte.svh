// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_ARRAY_BYTE_SVH
`define XLOGIC_PYTHON_PKG_ARRAY_BYTE_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_array.svh"

class array_byte extends array#(byte);
    extern static function items_t create(chandle pyobject);

    extern static function void create_ref(ref items_t items, input chandle pyobject);

    extern protected virtual function void set(ref items_t items, int index, input chandle value);

    extern protected virtual function void set_bytes(ref items_t items, const ref bytes_t bytes);

    local static array_byte m_instance;
endclass

function array_byte::items_t array_byte::create(chandle pyobject);
    create_ref(create, pyobject);
endfunction

function void array_byte::create_ref(ref items_t items, input chandle pyobject);
    array#(byte)::make(m_instance, items, pyobject);
endfunction

function void array_byte::set(ref items_t items, int index, input chandle value);
    items[index] = xlogic_python_pkg_PyLong_Check(value) ? xlogic_python_pkg_PyLong_AsLongLong(value) : 0;
endfunction

function void array_byte::set_bytes(ref items_t items, const ref bytes_t bytes);
    items = bytes;
endfunction

`endif /* XLOGIC_PYTHON_PKG_ARRAY_BYTE_SVH */
