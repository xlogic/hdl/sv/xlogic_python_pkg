// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_CREATE_SVH
`define XLOGIC_PYTHON_PKG_CREATE_SVH

`include "xlogic_python_pkg_value.svh"
`include "xlogic_python_pkg_interpreter.svh"
`include "xlogic_python_pkg_object_call.svh"

function automatic chandle create_ref(const ref string name, const ref value::array_t args, const ref value::map_t kwargs);
    return object_call_ref(interpreter::import_attribute(name, .callable(1'b1)), args, kwargs);
endfunction

function automatic chandle create(string name, value::array_t args = {}, value::map_t kwargs = '{default: null});
    return create_ref(name, args, kwargs);
endfunction

`endif /* XLOGIC_PYTHON_PKG_CREATE_SVH */
