// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_ARRAY_FLOAT_SVH
`define XLOGIC_PYTHON_PKG_ARRAY_FLOAT_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_array.svh"

class array_float#(type T = real) extends array#(T);
    extern static function items_t create(chandle pyobject);

    extern static function void create_ref(ref items_t items, input chandle pyobject);

    extern protected virtual function void set(ref items_t items, int index, input chandle value);

    extern protected virtual function void set_bytes(ref items_t items, const ref bytes_t bytes);

    local static array_float#(T) m_instance;
endclass

function array_float::items_t array_float::create(chandle pyobject);
    create_ref(create, pyobject);
endfunction

function void array_float::create_ref(ref items_t items, input chandle pyobject);
    array#(T)::make(m_instance, items, pyobject);
endfunction

function void array_float::set(ref items_t items, int index, input chandle value);
    items[index] = xlogic_python_pkg_PyFloat_Check(value) ? xlogic_python_pkg_PyFloat_AsDouble(value) : 0;
endfunction

function void array_float::set_bytes(ref items_t items, const ref bytes_t bytes);
    if (items.size() != bytes.size()) begin
        items = new[bytes.size()];
    end

    foreach (bytes[index]) begin
        items[index] = bytes[index];
    end
endfunction

`endif /* XLOGIC_PYTHON_PKG_ARRAY_FLOAT_SVH */
