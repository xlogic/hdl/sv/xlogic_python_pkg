// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_LIST_INTEGER_SVH
`define XLOGIC_PYTHON_PKG_LIST_INTEGER_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"

class list_integer#(type T = int) extends value;
    typedef T items_t[];

    extern function new(const ref T items[]);

    extern static function list_integer create(T items[] = {});

    extern static function list_integer create_ref(const ref T items[]);

    extern virtual function chandle to_python(ref chandle borrows[$]);

    extern static function items_t from(chandle handle, bit decref = 1'b1);

    extern static function void from_ref(ref T items[], input chandle handle, bit decref = 1'b1);

    protected chandle m_handle;
endclass

function list_integer::new(const ref T items[]);
    m_handle = xlogic_python_pkg_PyList_New(items.size());

    if (m_handle == null) begin
        return;
    end

    foreach (items[index]) begin
        xlogic_python_pkg_PyList_SET_ITEM(m_handle, index, xlogic_python_pkg_PyLong_FromLongLong(items[index]));
    end
endfunction

function list_integer list_integer::create(T items[] = {});
    create = new(items);
endfunction

function list_integer list_integer::create_ref(const ref T items[]);
    create_ref = new(items);
endfunction

function chandle list_integer::to_python(ref chandle borrows[$]);
    return m_handle;
endfunction

function list_integer::items_t list_integer::from(chandle handle, bit decref = 1'b1);
    from_ref(from, handle, decref);
endfunction

function void list_integer::from_ref(ref T items[], input chandle handle, bit decref = 1'b1);
    longint size;

    if ((handle == null) || (xlogic_python_pkg_PySequence_Check(handle) == 0)) begin
        items.delete();
        return;
    end

    size = xlogic_python_pkg_PySequence_Size(handle);

    if (items.size() != size) begin
        items = new [size];
    end

    foreach (items[index]) begin
        chandle item = xlogic_python_pkg_PySequence_GetItem(handle, index);

        if ((item != null) && (xlogic_python_pkg_PyLong_Check(item) != 0)) begin
            items[index] = xlogic_python_pkg_PyLong_AsLongLong(item);
        end

        xlogic_python_pkg_Py_XDECREF(item);
    end

    if (decref) begin
        xlogic_python_pkg_Py_DECREF(handle);
    end
endfunction

`endif /* XLOGIC_PYTHON_PKG_LIST_INTEGER_SVH */
