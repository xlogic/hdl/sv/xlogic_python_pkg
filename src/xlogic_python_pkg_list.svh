// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_LIST_SVH
`define XLOGIC_PYTHON_PKG_LIST_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"

class list extends value;
    extern function new(array_t items = {});

    extern static function list create(array_t items = {});

    extern static function list create_ref(const ref array_t items);

    extern static function list from_python(chandle handle);

    extern virtual function type_t get_type();

    extern virtual function xlogic_pkg::value::kind_t kind();

    extern virtual function int size();

    extern virtual function xlogic_pkg::value at(int index);

    extern virtual function xlogic_pkg::value get(string key);

    extern virtual function xlogic_pkg::value get_ref(const ref string key);

    extern virtual function void get_map(ref map_t items);

    extern virtual function void get_array(ref array_t items);

    extern virtual function map_t to_map();

    extern virtual function array_t to_array();

    extern virtual function void get_string(ref string value);

    extern virtual function string to_string();

    extern virtual function bit to_bit();

    extern virtual function chandle to_python(ref chandle borrows[$]);

    extern protected static function void _get_string(
        const ref array_t items,
        ref string value,
        input byte lhs = 'h5B,
        input byte rhs = 'h5D
    );

    protected array_t m_items;
endclass

function list::new(array_t items = {});
    m_items = items;
endfunction

function list list::create(array_t items = {});
    create = new();
    create.m_items = items;
endfunction

function list list::create_ref(const ref array_t items);
    create_ref = new();
    create_ref.m_items = items;
endfunction

function list list::from_python(chandle handle);
    from_python = new();
    from_python.m_items = new [xlogic_python_pkg_PyList_GET_SIZE(handle)];

    foreach (from_python.m_items[index]) begin
        from_python.m_items[index] = value::from_python(xlogic_python_pkg_PyList_GET_ITEM(handle, index));
    end
endfunction

function type_t list::get_type();
    return LIST;
endfunction

function xlogic_pkg::value::kind_t list::kind();
    return xlogic_pkg::value::ARRAY;
endfunction

function int list::size();
    return m_items.size();
endfunction

function xlogic_pkg::value list::at(int index);
    return m_items[index];
endfunction

function xlogic_pkg::value list::get(string key);
    return m_items[key.atoi()];
endfunction

function xlogic_pkg::value list::get_ref(const ref string key);
    return m_items[key.atoi()];
endfunction

function void list::get_map(ref map_t items);
    items.delete();

    foreach (m_items[index]) begin
        items[$sformatf("%0d", index)] = m_items[index];
    end
endfunction

function void list::get_array(ref array_t items);
    items = m_items;
endfunction

function value::map_t list::to_map();
    foreach (m_items[index]) begin
        to_map[$sformatf("%0d", index)] = m_items[index];
    end
endfunction

function value::array_t list::to_array();
    return m_items;
endfunction

function void list::get_string(ref string value);
    _get_string(m_items, value, 'h5B, 'h5D);
endfunction

function string list::to_string();
    _get_string(m_items, to_string, 'h5B, 'h5D);
endfunction

function bit list::to_bit();
    return m_items.size() != 0;
endfunction

function chandle list::to_python(ref chandle borrows[$]);
    chandle handle = xlogic_python_pkg_PyList_New(m_items.size());

    if (handle == null) begin
        return null;
    end

    foreach (m_items[index]) begin
        value item;

        if ($cast(item, m_items[index])) begin
            xlogic_python_pkg_PyList_SET_ITEM(handle, index, item.to_python(borrows));
        end else begin
            xlogic_python_pkg_PyList_SET_ITEM(handle, index, xlogic_python_pkg_Py_RETURN_NONE());
        end
    end

    return handle;
endfunction

function void list::_get_string(
    const ref array_t items,
    ref string value,
    input byte lhs = 'h5B,
    input byte rhs = 'h5D
);
    string item;

    if (items.size() == 0) begin
        value = {lhs, rhs};
        return;
    end

    value = {lhs};

    foreach (items[index]) begin
        items[index].get_string(item);

        if (items[index].kind() == xlogic_pkg::value::STRING) begin
            value = {value, $sformatf("%0s\"%0s\"", (index != 0) ? ", " : "", item)};
        end else begin
            value = {value, $sformatf("%0s%0s", (index != 0) ? ", " : "", item)};
        end
    end

    value = {value, rhs};
endfunction

`endif /* XLOGIC_PYTHON_PKG_LIST_SVH */
