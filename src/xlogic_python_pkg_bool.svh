// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_BOOL_SVH
`define XLOGIC_PYTHON_PKG_BOOL_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"

class bool extends value;
    extern function new(bit value = 1'b0);

    extern static function bool create(bit value = 1'b0);

    extern static function bool from_python(chandle handle);

    extern virtual function type_t get_type();

    extern virtual function xlogic_pkg::value::kind_t kind();

    extern virtual function void get_string(ref string value);

    extern virtual function string to_string();

    extern virtual function complex_t to_complex();

    extern virtual function real to_real();

    extern virtual function shortreal to_shortreal();

    extern virtual function bit to_bit();

    extern virtual function int to_int();

    extern virtual function longint to_longint();

    extern virtual function int unsigned to_int_unsigned();

    extern virtual function longint unsigned to_longint_unsigned();

    extern virtual function chandle to_python(ref chandle borrows[$]);

    protected bit m_value;
endclass

function bool::new(bit value = 1'b0);
    m_value = value;
endfunction

function bool bool::create(bit value = 1'b0);
    create = new(value);
endfunction

function bool bool::from_python(chandle handle);
    from_python = new(xlogic_python_pkg_PyBool_AsInt(handle));
endfunction

function type_t bool::get_type();
    return BOOL;
endfunction

function xlogic_pkg::value::kind_t bool::kind();
    return xlogic_pkg::value::BOOLEAN;
endfunction

function void bool::get_string(ref string value);
    value = $sformatf("%0d", m_value);
endfunction

function string bool::to_string();
    return $sformatf("%0d", m_value);
endfunction

function complex_t bool::to_complex();
    return '{re: real'(m_value), default: 0};
endfunction

function real bool::to_real();
    return real'(unsigned'(m_value));
endfunction

function shortreal bool::to_shortreal();
    return shortreal'(unsigned'(m_value));
endfunction

function bit bool::to_bit();
    return m_value;
endfunction

function int bool::to_int();
    return int'(unsigned'(m_value));
endfunction

function longint bool::to_longint();
    return longint'(unsigned'(m_value));
endfunction

function int unsigned bool::to_int_unsigned();
    return int_unsigned_t'(m_value);
endfunction

function longint unsigned bool::to_longint_unsigned();
    return longint_unsigned_t'(m_value);
endfunction

function chandle bool::to_python(ref chandle borrows[$]);
    return xlogic_python_pkg_PyBool_FromLong(int'(unsigned'(m_value)));
endfunction

`endif /* XLOGIC_PYTHON_PKG_BOOL_SVH */
