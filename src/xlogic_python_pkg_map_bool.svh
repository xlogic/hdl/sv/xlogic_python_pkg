// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_MAP_BOOL_SVH
`define XLOGIC_PYTHON_PKG_MAP_BOOL_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_map.svh"

class map_bool#(type T = bit) extends map#(T);
    extern static function items_t create(chandle pyobject);

    extern static function void create_ref(ref items_t items, input chandle pyobject);

    extern protected virtual function void set(ref items_t items, const ref string key, input chandle value);

    local static map_bool#(T) m_instance;
endclass

function map_bool::items_t map_bool::create(chandle pyobject);
    create_ref(create, pyobject);
endfunction

function void map_bool::create_ref(ref items_t items, input chandle pyobject);
    map#(T)::make(m_instance, items, pyobject);
endfunction

function void map_bool::set(ref items_t items, const ref string key, input chandle value);
    items[key] = xlogic_python_pkg_PyBool_Check(value) ? xlogic_python_pkg_PyBool_AsInt(value) : 0;
endfunction

`endif /* XLOGIC_PYTHON_PKG_MAP_BOOL_SVH */
