// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_VALUE_FROM_PYTHON_SVH
`define XLOGIC_PYTHON_PKG_VALUE_FROM_PYTHON_SVH

`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_exception.svh"
`include "xlogic_python_pkg_none.svh"
`include "xlogic_python_pkg_bool.svh"
`include "xlogic_python_pkg_number.svh"
`include "xlogic_python_pkg_complex.svh"
`include "xlogic_python_pkg_tuple.svh"
`include "xlogic_python_pkg_list.svh"
`include "xlogic_python_pkg_set.svh"
`include "xlogic_python_pkg_dict.svh"
`include "xlogic_python_pkg_bytes.svh"
`include "xlogic_python_pkg_byte_array.svh"

function value value::from_python(chandle handle);
    if (handle == null) begin
        return exception::create("Python object null detected");
    end

    if (xlogic_python_pkg_PyNone_Check(handle) != 0) begin
        return none::create();
    end

    if (xlogic_python_pkg_PyBool_Check(handle) != 0) begin
        return bool::from_python(handle);
    end

    if (xlogic_python_pkg_PyLong_Check(handle) != 0) begin
        return number::from_python(handle);
    end

    if (xlogic_python_pkg_PyUnicode_Check(handle) != 0) begin
        return str::from_python(handle);
    end

    if (xlogic_python_pkg_PyComplex_Check(handle) != 0) begin
        return complex::from_python(handle);
    end

    if (xlogic_python_pkg_PyBytes_Check(handle) != 0) begin
        return bytes::from_python(handle);
    end

    if (xlogic_python_pkg_PyByteArray_Check(handle) != 0) begin
        return byte_array::from_python(handle);
    end

    if (xlogic_python_pkg_PyTuple_Check(handle) != 0) begin
        return tuple::from_python(handle);
    end

    if (xlogic_python_pkg_PyList_Check(handle) != 0) begin
        return list::from_python(handle);
    end

    if (xlogic_python_pkg_PySet_Check(handle) != 0) begin
        return set::from_python(handle);
    end

    if (xlogic_python_pkg_PyDict_Check(handle) != 0) begin
        return dict::from_python(handle);
    end

    if (xlogic_python_pkg_PyMapping_Check(handle) != 0) begin
        return dict::from_python_mapping(handle);
    end

    if (xlogic_python_pkg_PySequence_Check(handle) != 0) begin
        return tuple::from_python_sequence(handle);
    end

    return exception::create("Unsupported Python type");
endfunction

`endif /* XLOGIC_PYTHON_PKG_VALUE_FROM_PYTHON_SVH */
