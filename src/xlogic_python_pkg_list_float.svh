// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_LIST_FLOAT_SVH
`define XLOGIC_PYTHON_PKG_LIST_FLOAT_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"

class list_float#(type T = real) extends value;
    extern function new(const ref T items[]);

    extern static function list_float create(T items[] = {});

    extern static function list_float create_ref(const ref T items[]);

    extern virtual function chandle to_python(ref chandle borrows[$]);

    protected chandle m_handle;
endclass

function list_float::new(const ref T items[]);
    m_handle = xlogic_python_pkg_PyList_New(items.size());

    if (m_handle == null) begin
        return;
    end

    foreach (items[index]) begin
        xlogic_python_pkg_PyList_SET_ITEM(m_handle, index, xlogic_python_pkg_PyFloat_FromDouble(items[index]));
    end
endfunction

function list_float list_float::create(T items[] = {});
    create = new(items);
endfunction

function list_float list_float::create_ref(const ref T items[]);
    create_ref = new(items);
endfunction

function chandle list_float::to_python(ref chandle borrows[$]);
    return m_handle;
endfunction

`endif /* XLOGIC_PYTHON_PKG_LIST_FLOAT_SVH */
