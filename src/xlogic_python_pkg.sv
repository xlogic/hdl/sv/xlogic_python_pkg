// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

// Package: xlogic_python_pkg
// It allows to use Python directly from SystemVerilog.
package xlogic_python_pkg;
    `include "xlogic_python_pkg_dpi.svh"
    `include "xlogic_python_pkg_value.svh"
    `include "xlogic_python_pkg_utils.svh"
    `include "xlogic_python_pkg_object.svh"
    `include "xlogic_python_pkg_exception.svh"
    `include "xlogic_python_pkg_none.svh"
    `include "xlogic_python_pkg_str.svh"
    `include "xlogic_python_pkg_bool.svh"
    `include "xlogic_python_pkg_bytes.svh"
    `include "xlogic_python_pkg_byte_array.svh"
    `include "xlogic_python_pkg_number.svh"
    `include "xlogic_python_pkg_float.svh"
    `include "xlogic_python_pkg_complex.svh"
    `include "xlogic_python_pkg_set.svh"
    `include "xlogic_python_pkg_list.svh"
    `include "xlogic_python_pkg_list_bool.svh"
    `include "xlogic_python_pkg_list_float.svh"
    `include "xlogic_python_pkg_list_string.svh"
    `include "xlogic_python_pkg_list_integer.svh"
    `include "xlogic_python_pkg_tuple.svh"
    `include "xlogic_python_pkg_dict.svh"
    `include "xlogic_python_pkg_dict_bool.svh"
    `include "xlogic_python_pkg_dict_float.svh"
    `include "xlogic_python_pkg_dict_string.svh"
    `include "xlogic_python_pkg_dict_integer.svh"
    `include "xlogic_python_pkg_interpreter.svh"
    `include "xlogic_python_pkg_object_call.svh"
    `include "xlogic_python_pkg_array.svh"
    `include "xlogic_python_pkg_array_byte.svh"
    `include "xlogic_python_pkg_array_string.svh"
    `include "xlogic_python_pkg_array_float.svh"
    `include "xlogic_python_pkg_array_integer.svh"
    `include "xlogic_python_pkg_map.svh"
    `include "xlogic_python_pkg_map_bool.svh"
    `include "xlogic_python_pkg_map_string.svh"
    `include "xlogic_python_pkg_map_float.svh"
    `include "xlogic_python_pkg_map_integer.svh"
    `include "xlogic_python_pkg_call.svh"
    `include "xlogic_python_pkg_create.svh"
    `include "xlogic_python_pkg_method.svh"
    `include "xlogic_python_pkg_value_from_python.svh"
endpackage
