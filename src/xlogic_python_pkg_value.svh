// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_VALUE_SVH
`define XLOGIC_PYTHON_PKG_VALUE_SVH

`include "xlogic_python_pkg_types.svh"

virtual class value extends xlogic_pkg::value_base;
    extern virtual function type_t get_type();

    pure virtual function chandle to_python(ref chandle borrows[$]);

    extern static function value from_python(chandle handle);
endclass

function type_t value::get_type();
    return NONE;
endfunction

`endif /* XLOGIC_PYTHON_PKG_VALUE_SVH */
