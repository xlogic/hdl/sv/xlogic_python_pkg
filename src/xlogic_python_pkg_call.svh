// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_CALL_SVH
`define XLOGIC_PYTHON_PKG_CALL_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"
`include "xlogic_python_pkg_tuple.svh"
`include "xlogic_python_pkg_dict.svh"
`include "xlogic_python_pkg_interpreter.svh"
`include "xlogic_python_pkg_object_call.svh"

function automatic chandle pycall_ref(const ref string name, const ref value::array_t args, const ref value::map_t kwargs);
    return object_call_ref(interpreter::import_attribute(name), args, kwargs);
endfunction

function automatic chandle pycall(string name, value::array_t args = {}, value::map_t kwargs = '{default: null});
    return object_call_ref(interpreter::import_attribute(name), args, kwargs);
endfunction

function automatic value call_ref(const ref string name, const ref value::array_t args, const ref value::map_t kwargs);
    chandle result = object_call_ref(interpreter::import_attribute(name), args, kwargs);

    call_ref = value::from_python(result);

    xlogic_python_pkg_Py_DECREF(result);
endfunction

function automatic value call(string name, value::array_t args = {}, value::map_t kwargs = '{default: null});
    return call_ref(name, args, kwargs);
endfunction

`endif /* XLOGIC_PYTHON_PKG_CALL_SVH */
