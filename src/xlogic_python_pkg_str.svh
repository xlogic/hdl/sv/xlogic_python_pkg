// SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

`ifndef XLOGIC_PYTHON_PKG_STR_SVH
`define XLOGIC_PYTHON_PKG_STR_SVH

`include "xlogic_python_pkg_dpi.svh"
`include "xlogic_python_pkg_types.svh"
`include "xlogic_python_pkg_value.svh"

class str extends value;
    extern function new(string value = "");

    extern static function str create(string value = "");

    extern static function str create_ref(const ref string value);

    extern static function str from_python(chandle handle);

    extern virtual function type_t get_type();

    extern virtual function xlogic_pkg::value::kind_t kind();

    extern virtual function int size();

    extern virtual function void get_string(ref string value);

    extern virtual function string to_string();

    extern virtual function complex_t to_complex();

    extern virtual function real to_real();

    extern virtual function shortreal to_shortreal();

    extern virtual function bit to_bit();

    extern virtual function int to_int();

    extern virtual function longint to_longint();

    extern virtual function int unsigned to_int_unsigned();

    extern virtual function longint unsigned to_longint_unsigned();

    extern virtual function chandle to_python(ref chandle borrows[$]);

    protected string m_value;
endclass

function str::new(string value = "");
    m_value = value;
endfunction

function str str::create(string value = "");
    return create_ref(value);
endfunction

function str str::create_ref(const ref string value);
    create_ref = new();

    create_ref.m_value = value;
endfunction

function str str::from_python(chandle handle);
    from_python = new();

    if (xlogic_python_pkg_PyUnicode_Check(handle) == 0) begin
        return from_python;
    end

    handle = xlogic_python_pkg_PyUnicode_AsASCIIString(handle);

    if (handle == null) begin
        return from_python;
    end

    from_python.m_value = xlogic_python_pkg_PyBytes_AsString(handle);

    xlogic_python_pkg_Py_DECREF(handle);
endfunction

function type_t str::get_type();
    return STR;
endfunction

function xlogic_pkg::value::kind_t str::kind();
    return xlogic_pkg::value::STRING;
endfunction

function int str::size();
    return m_value.len();
endfunction

function void str::get_string(ref string value);
    value = m_value;
endfunction

function string str::to_string();
    return m_value;
endfunction

function complex_t str::to_complex();
    return '{re: xlogic_pkg::string_float#(real)::convert(m_value), default: 0};
endfunction

function real str::to_real();
    return xlogic_pkg::string_float#(real)::convert(m_value);
endfunction

function shortreal str::to_shortreal();
    return xlogic_pkg::string_float#(shortreal)::convert(m_value);
endfunction

function bit str::to_bit();
    case (m_value.tolower())
    "true", "1", "y", "yes", "on", "enable", "enabled": return 1'b1;
    default: return 1'b0;
    endcase
endfunction

function int str::to_int();
    return xlogic_pkg::string_integer#(int)::convert(m_value);
endfunction

function longint str::to_longint();
    return xlogic_pkg::string_integer#(longint)::convert(m_value);
endfunction

function int unsigned str::to_int_unsigned();
    return xlogic_pkg::string_integer#(int unsigned)::convert(m_value);
endfunction

function longint unsigned str::to_longint_unsigned();
    return xlogic_pkg::string_integer#(longint unsigned)::convert(m_value);
endfunction

function chandle str::to_python(ref chandle borrows[$]);
    return xlogic_python_pkg_PyUnicode_FromString(m_value);
endfunction

`endif /* XLOGIC_PYTHON_PKG_STR_SVH */
